open Alcotest;
let tests = [
  test_case("Concurrency (one domain)", `Quick, () => {
    let mem = ref(1)
    let finished = ref(false)

    Coppola.run(~domain_count=1, () => {
      let actor1 = Coppola.spawn()
      let actor2 = Coppola.spawn()
      let actor3 = Coppola.spawn()
      Coppola.send(actor1, () => {
        let _status = Effect.perform @@ Coppola.Async.CreateProcess({
          path: "/usr/bin/sleep",
          args: [| "/usr/bin/sleep", "0.01"|],
          stdin: Unix.stdin,
          stdout: Unix.stdout,
          stderr: Unix.stderr,
          handler: _status => mem := mem^ * 2
        })
      })
      Coppola.send(actor2, () => {
        let _status = Effect.perform @@ Coppola.Async.CreateProcess({
          path: "/usr/bin/sleep",
          args: [| "/usr/bin/sleep", "0.03"|],
          stdin: Unix.stdin,
          stdout: Unix.stdout,
          stderr: Unix.stderr,
          handler: (_status => { mem := mem^ * 3; finished := true })
        })
      })
      Coppola.send(actor3, () => {
        let _status = Effect.perform @@ Coppola.Async.CreateProcess({
          path: "/usr/bin/sleep",
          args: [| "/usr/bin/sleep", "0.02"|],
          stdin: Unix.stdin,
          stdout: Unix.stdout,
          stderr: Unix.stderr,
          handler: _status => mem := mem^ + 10
        })
      })
      mem := mem^ + 2
      while(!finished^) { Effect.perform @@ Coppola.Async.Sleep({ timeout_ms: 10 }) }
    })
  check(int)("mem", 48, mem^)
  }),
  test_case("Concurrency (many domains)", `Quick, () => {
    let mem = ref(1)
    let finished = ref(false)

    Coppola.run(~domain_count=8, () => {
      let actor1 = Coppola.spawn()
      let actor2 = Coppola.spawn()
      let actor3 = Coppola.spawn()
      Coppola.send(actor1, () => {
        Effect.perform @@ Coppola.Async.CreateProcess({
          path: "/usr/bin/sleep",
          args: [| "/usr/bin/sleep", "0.01"|],
          stdin: Unix.stdin,
          stdout: Unix.stdout,
          stderr: Unix.stderr,
          handler: _status => mem := mem^ * 2
        })
      })
      Coppola.send(actor2, () => {
        Effect.perform @@ Coppola.Async.CreateProcess({
          path: "/usr/bin/sleep",
          args: [| "/usr/bin/sleep", "0.03"|],
          stdin: Unix.stdin,
          stdout: Unix.stdout,
          stderr: Unix.stderr,
          handler: (_status => { mem := mem^ * 3; finished := true })
        })
      })
      Coppola.send(actor3, () => {
        Effect.perform @@ Coppola.Async.CreateProcess({
          path: "/usr/bin/sleep",
          args: [| "/usr/bin/sleep", "0.02"|],
          stdin: Unix.stdin,
          stdout: Unix.stdout,
          stderr: Unix.stderr,
          handler: _status => mem := mem^ + 10
        })
      })
      mem := mem^ + 2
      while(!finished^) { Effect.perform @@ Coppola.Async.Sleep({ timeout_ms: 10 }) }
    })
  check(int)("mem", 48, mem^)
  }),
  test_case("Errors", `Quick, () => {
    check_raises("fails on invalid executable", Unix.Unix_error(Unix.EACCES, "create_process", "/"), () => Coppola.run(() => {
      Effect.perform @@ Coppola.Async.CreateProcess({
        path: "/",
        args: [||],
        stdin: Unix.stdin,
        stdout: Unix.stdout,
        stderr: Unix.stderr,
        handler: _ => (),
      })
    }))
  }),
]
