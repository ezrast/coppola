Alcotest.run("Coppola", [
  ("Actors", Actors.tests),
  ("Error Handlers", ErrorHandlers.tests),
  ("Processes", Processes.tests),
  ("TcpSockets", TcpSockets.tests),

  ("Std - Io", StdIo.tests),
]);
