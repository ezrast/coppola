type exn += | Fail1 | Fail2 | Fail3;

module Failer {
  type t = Actor(Coppola.actor(ref(int)))
  let spawn(data: ref(int)): t = Actor(Coppola.spawn(data))

  let send(Actor(actor), fn) = Coppola.send(actor, data => {
    try (fn(data)) {
    | Fail1 => data := 1 + data^
    | Fail2 => raise(Fail1)
    | exn => raise(exn)
    }
  })
}

open Alcotest;
let tests = [
  test_case("Handled errors", `Quick, () => {
    let ref = ref(0);

    Coppola.run(() => {
      let actor = Failer.spawn(ref)
      Failer.send(actor, ref => ref := 3)
      Failer.send(actor, _ => raise(Fail1))
    })
    check(int)("Fail1", 4, ref^)
  }),
  test_case("Handled errors in continuation", `Quick, () => {
    let ref = ref(0);

    Coppola.run(() => {
      let actor = Failer.spawn(ref)
      Failer.send(actor, ref => ref := 3)
      Failer.send(actor, _ => { Effect.perform @@ Coppola.Async.Sleep({ timeout_ms: 1 }); raise(Fail1) })
    })
    check(int)("Fail1", 4, ref^)
  }),
  test_case("Errors are handled immediately", `Quick, () => {
    let ref = ref(0);

    Coppola.run(() => {
      let actor = Failer.spawn(ref)
      Failer.send(actor, _ref => {
        Failer.send(actor, ref => ref := 10 * ref^)
        raise(Fail1)
      })
    })
    check(int)("Fail1 * 10", 10, ref^)
  }),
  test_case("Unhandled Errors", `Quick, () => {
    let ref = ref(0);

    check_raises("Fail2", Fail1, () => {
      Coppola.run(() => {
        Failer.send(Failer.spawn(ref), _ => raise(Fail2))
      })
    })
    check_raises("Fail3", Fail3, () => {
      Coppola.run(() => {
        Failer.send(Failer.spawn(ref), _ => raise(Fail3))
      })
    })

  }),
]
