open Coppola.Infix

module Server {
  let spawn(port) = Coppola.spawn((port, ref(None)))

  let start((port, state)) = {
    if (state^ == None) {
      let sock = Unix.socket(~cloexec=true, Unix.PF_INET, Unix.SOCK_STREAM, 0)
      Unix.setsockopt(sock, Unix.SO_REUSEPORT, true)
      Unix.bind(sock, Unix.ADDR_INET(Unix.inet_addr_of_string("127.0.0.234"), port))
      Effect.perform @@ Coppola.Async.Listen({ fd: sock, handler: (client, _clientaddr) => {
        let msg = Bytes.of_string("abcde");
        let _bytes_written = Effect.perform @@ Coppola.Async.Write({ fd: client, bytes: msg, offset: 0, length: Bytes.length(msg), timeout_ms: 100 });
      }});
      state := Some(sock)
    }
  }

  let stop((_port, state)) = {
    switch state^ {
    | None => ()
    | Some(sock) => Effect.perform @@ Coppola.Async.Close({ fd: sock }); state := None
    }
  }
}

module Client {
  let spawn(port: int) = Coppola.spawn(port)

  let go(bytes, port) = {
    let client = Unix.socket(~cloexec=true, Unix.PF_INET, Unix.SOCK_STREAM, 0);
    Unix.connect(client, Unix.ADDR_INET(Unix.inet_addr_of_string("127.0.0.234"), port)) // TODO this blocks; replace with a Coppola version
    assert(0 < Effect.perform @@ Coppola.Async.Read({ fd: client, bytes, offset: 0, length: Bytes.length(bytes), timeout_ms: 100 }))
    Effect.perform @@ Coppola.Async.Sleep({ timeout_ms: 100 })
    Unix.close(client)
  }
}

open Alcotest
let tests = [
  test_case("simple client/server", `Quick, () => {
    let buf = Bytes.create(6);
    Coppola.run(() => {
      let server = Server.spawn(65432);
      let client = Client.spawn(65432)

      Server.(server |>> start);
      Client.(client |>> go(buf));
      Effect.perform @@ Coppola.Async.Sleep({ timeout_ms: 100 })
      Server.(server >> stop);
    })
    check(bytes)("buf", Bytes.of_string("abcde\000"), buf)
  }),
  test_case("this doesn't raise EBADF", `Quick, () => {
    let buf = Bytes.create(6);
    Coppola.run(() => {
      let server = Server.spawn(65433);
      let client = Client.spawn(65433);

      Server.(server |>> start);
      Client.(client |>> go(buf));
      Server.(server >> stop);
    })

    check(bytes)("buf", Bytes.of_string("abcde\000"), buf)
  }),
  test_case("Out-of-scope actors don't get cleaned up too soon", `Quick, () => {
    let buf = Bytes.create(6);
    Coppola.run(() => {
      Server.spawn(65434) |>> Server.start;
      Gc.full_major()
      let client = Client.spawn(65434);
      Client.(client |>> go(buf));
    })
  })
]
