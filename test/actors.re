open Coppola.Infix;

module IOActor {
  let spawn(~bytes_transferred=ref(-1), buf: bytes, fd: Unix.file_descr) = Coppola.spawn((buf, fd, bytes_transferred));

  let read(~timeout_ms=2, (bytes, fd, bytes_transferred)) = bytes_transferred := Effect.perform @@ Coppola.Async.Read({ fd, bytes, offset: 0, length: Bytes.length(bytes), timeout_ms })
  let write(~timeout_ms=2, (bytes, fd, bytes_transferred)) = bytes_transferred := Effect.perform @@ Coppola.Async.Write({ fd, bytes, offset: 0, length: Bytes.length(bytes), timeout_ms })
  let get_bytes_transferred((_buf, _fd, bytes_transferred)) = bytes_transferred
}

open Alcotest;
let tests = [
  test_case("Concurrency", `Quick, () => {
    let atom = Atomic.make(0);

    let incr(atom) = {
      let xx = Atomic.get(atom)
      Effect.perform @@ Coppola.Async.Sleep({ timeout_ms: 5 })
      Atomic.set(atom, xx + 1)
    }

    Coppola.run(~domain_count=1, () => {
      for (_ in 1 to 100) {
        Coppola.spawn(atom) >> incr
      };
    })
    check(int)("atom", 1, Atomic.get(atom))
  }),
  test_case("Wakeup order", `Quick, () => {
    let mutex = Mutex.create()
    let list = ref([]);
    Coppola.run @@ () => {
      [3, 4, 8, 2, 1, 9, 5, 7, 6, 10] |> List.iteri((idx, timeout_interval: int) => {
        Coppola.spawn() >> () => {
          Effect.perform @@ Coppola.Async.Sleep({ timeout_ms: 2 * timeout_interval })
          Mutex.protect(mutex, () => { list := [idx, ...list^] })
        }
      });
    }
    check(Alcotest.list(int))("result order", [9, 5, 2, 7, 8, 6, 1, 0, 3, 4], list^)
  }),
  test_case("Reading", `Quick, () => {
    let (rr, ww) = Unix.pipe()
    let wc = Unix.out_channel_of_descr(ww)
    Out_channel.output_string(wc, "hello")
    Stdlib.flush(wc)

    let buf = Bytes.make(32, Char.chr(0))
    let bytes_transferred = ref(-1);
    Coppola.run(() => {
      let actor = IOActor.spawn(~bytes_transferred, buf, rr);
      IOActor.(actor >> read);
    })

    check(int)("bytes read", 5, bytes_transferred^)
    check(string)("buffer after", "hello", buf |> Bytes.to_seq |> Seq.take_while((!=)('\000')) |> String.of_seq)
  }),
  test_case("Concurrent reads", `Quick, () => {
    let buf = Bytes.make(32, Char.chr(0))

    let (closeme, _) = Unix.pipe()
    Unix.close(closeme);

    // the notification pipe is the first thing we open on spawn, so it's guaranteed to have the same fd as the last thing we closed
    check_raises("fails on concurrent reads to the notification pipe", Coppola.Errors.FileDescriptorBelongsToCoppola, () => Coppola.run(() => {
      let actor1 = IOActor.spawn(buf, closeme);
      IOActor.(actor1 >> read);
    }))

    let (rr, _ww) = Unix.pipe()
    check_raises("fails on concurrent reads", Coppola.Errors.FileDescriptorInUse, () => Coppola.run(() => {
      let actor1 = IOActor.spawn(buf, rr);
      let actor2 = IOActor.spawn(buf, rr);

      IOActor.(actor1 >> read);
      IOActor.(actor2 >> read);
    }))
  }),
  test_case("Error in second domain doesn't lock up", `Quick, () => {
    check_raises("fails", Failure("x"), () => Coppola.run(() => {
      let actor1 = Coppola.spawn()
      let actor2 = Coppola.spawn()

      Coppola.send(actor1, () => { Unix.sleepf(0.001) });
      Coppola.send(actor2, () => { raise(Failure("x")) });
    }))
  }),
  test_case("Read timeout", `Quick, () => {
    let (rr, _ww) = Unix.pipe()

    let buf = Bytes.make(32, Char.chr(0))
    let bytes_transferred = ref(-1);
    Coppola.run(() => {
      let actor = IOActor.spawn(~bytes_transferred, buf, rr);
      actor >> IOActor.read(~timeout_ms=10);
    })

    check(int)("bytes read", -1, bytes_transferred^)
    check(string)("buffer after", "", buf |> Bytes.to_seq |> Seq.take_while((!=)('\000')) |> String.of_seq)
  }),
  test_case("Writing", `Quick, () => {
    let (rr, ww) = Unix.pipe()

    let input_buf = "fruit flies like a banana" |> String.to_bytes
    let bytes_transferred = ref(-1);
    Coppola.run(() => {
      let write_actor = IOActor.spawn(~bytes_transferred, input_buf, ww);
      write_actor >> IOActor.write;
    })
    Unix.close(ww)

    let str = rr |> Unix.in_channel_of_descr |> In_channel.input_all
    check(int)("bytes written", 25, bytes_transferred^)
    check(string)("buffer after", "fruit flies like a banana", str)
  }),
  test_case("Concurrent writes", `Quick, () => {
    let (_rr, ww) = Unix.pipe()
    Unix.set_nonblock(ww);
    let buf = Bytes.make(500000, Char.chr(0));
    assert(Unix.write(ww, buf, 0, Bytes.length(buf)) > 0);

    check_raises("fails on concurrent writes", Coppola.Errors.FileDescriptorInUse, () => Coppola.run(() => {
      let actor1 = IOActor.spawn(buf, ww);
      let actor2 = IOActor.spawn(buf, ww);

      actor1 >> IOActor.write(~timeout_ms=10);
      actor2 >> IOActor.write(~timeout_ms=10);
    }))
  }),
  test_case("Closing file descriptors", `Quick, () => {
    let (closeme, _) = Unix.pipe()
    Unix.close(closeme);

    // the notification pipe is the first thing we open on spawn, so it's guaranteed to have the same fd as the last thing we closed
    check_raises("internal descriptor will not close", Coppola.Errors.FileDescriptorBelongsToCoppola, () => {
      Coppola.run(() => {
        Effect.perform @@ Coppola.Async.Close({ fd: closeme })
        Coppola.spawn() >> Fun.id // if the notification pipe gets closed, some kind of error will happen
      })
    })

    let (rr, _ww) = Unix.pipe()
    let bytes_read = ref(-9999);
    let bytes = Bytes.make(10, '\000');

    Coppola.run(() => {
      Coppola.spawn() >> () => {
        bytes_read := Effect.perform @@ Coppola.Async.Read({fd: rr, bytes, offset: 0, length: 10, timeout_ms: 30 });
        ()
      }
      Effect.perform @@ Coppola.Async.Sleep({ timeout_ms: 10 })
      Effect.perform @@ Coppola.Async.Close({ fd: rr })
      Effect.perform @@ Coppola.Async.Sleep({ timeout_ms: 10 })
    });
    check(int)("bytes read is -2 on close", -2, bytes_read^);

    // Coppola.run(() => {
    //   // Effect.perform @@ Coppola.Async.Close({ fd: rr })
    //   ignore @@ Effect.perform @@ Coppola.Async.Read({fd: rr, bytes, offset: 0, length: 10, timeout_ms: 11 })
    // })
  }),
  test_case("Messages to self", `Quick, () => {
    let x = ref(0)

    Coppola.run @@ () => {
      let actor1 = Coppola.spawn()

      actor1 >> () => {
        actor1 >> () => { x := x^ + 1 };
      };
    }

    check(int)("ref after", 1, x^)
  }),
  test_case("Cleanup", `Quick, () => {
    Coppola.run(~domain_count=1) @@ () => {
      check(int)("actor count 1", 1, Effect.perform @@ Coppola.Async.Diagnostics |> List.assoc("actor_count"))
      Coppola.spawn() |>> () => {
        check(int)("actor count 2", 2, Effect.perform @@ Coppola.Async.Diagnostics |> List.assoc("actor_count"))
      }
      check(int)("actor count 3", 2, Effect.perform @@ Coppola.Async.Diagnostics |> List.assoc("actor_count"))
      Gc.compact()
      check(int)("actor count 4", 1, Effect.perform @@ Coppola.Async.Diagnostics |> List.assoc("actor_count"))
    }
  }),
]
