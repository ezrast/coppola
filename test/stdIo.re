open Alcotest

let tmppath = "/tmp/coppola-std-test-" ++ string_of_int(Unix.getpid())
let test_case_with_files(name, fn) = {
  test_case(name, `Quick, () => {
    let file = Out_channel.open_gen([Out_channel.Open_creat, Out_channel.Open_excl, Out_channel.Open_wronly], 0o644, tmppath);
    Out_channel.output_string(file, "hello\n\nxyz")
    Out_channel.close(file)
    Fun.protect(~finally=() => Sys.remove(tmppath), fn)
  })
}

let reader_tests(factory) = [
  test_case_with_files("read_line", () => {
    Coppola.run(() => {
      let inf = factory()
      check(option(string))("string1", Some("hello"), inf#read_line)
      check(option(string))("string2", Some(""), inf#read_line)
      check(option(string))("string3", Some("xyz"), inf#read_line)
      check(option(string))("string4", None, inf#read_line)
      check(option(string))("string5", None, inf#read_line)
    })
  }),
  // test_case_with_files("iter_max", () => { // TODO
  // }),
  test_case_with_files("read_max", () => {
    Coppola.run(() => {
      let inf = factory()
      check(option(string))("string1", Some("hello\n\nx"), inf#read_max(8))
      check(option(string))("string2", Some("yz"), inf#read_max(8))
      check(option(string))("string3", None, inf#read_max(8))
    })
  }),
  test_case_with_files("read_char", () => {
    Coppola.run(() => {
      let inf = factory();
      check(option(char))("char1", Some('h'), inf#read_char)
      check(option(char))("char2", Some('e'), inf#read_char)
      check(option(char))("char3", Some('l'), inf#read_char)
      check(option(char))("char4", Some('l'), inf#read_char)
      check(option(char))("char5", Some('o'), inf#read_char)
      check(option(char))("char6", Some('\n'), inf#read_char)
      check(option(char))("char7", Some('\n'), inf#read_char)
      check(option(char))("char8", Some('x'), inf#read_char)
      check(option(char))("char9", Some('y'), inf#read_char)
      check(option(char))("char10", Some('z'), inf#read_char)
      check(option(char))("char11", None, inf#read_char)
      check(option(char))("char12", None, inf#read_char)
      check(option(char))("char13", None, inf#read_char)
      inf#close;
    })
  }),
  test_case_with_files("read_line_sep", () => {
    Coppola.run(() => {
      let inf = factory()
      check(option(string))("string1", Some("hello\n"), inf#read_line_sep)
      check(option(string))("string2", Some("\n"), inf#read_line_sep)
      check(option(string))("string3", Some("xyz"), inf#read_line_sep)
      check(option(string))("string4", None, inf#read_line_sep)
      check(option(string))("string5", None, inf#read_line_sep)
    })
  }),
  test_case_with_files("line_seq", () => {
    Coppola.run(() => {
      let inf = factory()
      check(list(string))("list1", ["hello", "", "xyz"], inf#line_seq |> List.of_seq)
      check(list(string))("list2", [], inf#line_seq |> List.of_seq)
    })
  }),
  test_case_with_files("line_seq_sep", () => {
    Coppola.run(() => {
      let inf = factory()
      check(list(string))("list1", ["hello\n", "\n", "xyz"], inf#line_seq_sep |> List.of_seq)
      check(list(string))("list2", [], inf#line_seq |> List.of_seq)
    })
  }),
  test_case_with_files("seek", () => {
    Coppola.run(() => {
      let inf = factory();
      inf#seek(2);
      check(option(string))("seek 1", Some("llo"), inf#read_line)
      inf#seek(2);
      check(option(string))("seek 2", Some("llo"), inf#read_line)
    })
  }),
  test_case_with_files("seek_end", () => {
    Coppola.run(() => {
      let inf = factory();
      inf#seek_end(-8);
      check(option(string))("seek_end 1", Some("llo"), inf#read_line)
      inf#seek_end(-8);
      check(option(string))("seek_end 2", Some("llo"), inf#read_line)
    })
  }),
  test_case_with_files("closing", () => {
    Coppola.run(() => {
      let inf = factory();
      inf#close;
      // TODO figure out a consistent error handling paradigm; it makes
      // no sense for read and seek to behave differently here
      check(option(char))("read_char", None, inf#read_char);
      check(option(string))("read_line", None, inf#read_line);
      check(option(string))("read_line_sep", None, inf#read_line_sep);
      check_raises("seek", Unix.Unix_error(Unix.EBADF, "lseek", ""), () => inf#seek(1))
    })
  }),
]

// TODO test writers

let tests = List.concat([
  reader_tests(() => new Coppola.Std.File.reader(tmppath)),
  reader_tests(() => new Coppola.Std.File.rw(tmppath)),
]);
