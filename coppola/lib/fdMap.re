include Map.Make({
  type t = Unix.file_descr;
  let compare(fd1: t, fd2: t) = Int.compare(Iomux.Util.fd_of_unix(fd1), Iomux.Util.fd_of_unix(fd2)) }
);
