module CueSet = Set.Make(Cue);

module DispatcherId {
  type t = DispatcherId(int);
  let hash(id) = {
    let DispatcherId(int) = id
    Int.hash(int)
  }

  let equal(one, two) = one == two

  let compare(one, two) = {
    let DispatcherId(int1) = one
    let DispatcherId(int2) = two
    Int.compare(int1, int2)
  }
};

type t = {
  dispatcher_id: DispatcherId.t,
  cue_queue: CueSet.t,
  active: bool,
}

let compare(d1: t, d2: t) = {
  switch (d1.active == d2.active) {
  | true =>
    let i1 = CueSet.min_elt_opt(d1.cue_queue) |> Option.map((item: Cue.t) => item.cue_id) |> Option.value(~default=max_int);
    let i2 = CueSet.min_elt_opt(d2.cue_queue) |> Option.map((item: Cue.t) => item.cue_id) |> Option.value(~default=max_int);
    switch (i1 == i2) {
    | true => DispatcherId.compare(d1.dispatcher_id, d2.dispatcher_id)
    | false => Int.compare(i1, i2)
    }
  | false => Bool.compare(d1.active, d2.active)
  }
}

let string_of_dispatcher(dis: t) = {
  let DispatcherId(id_int) = dis.dispatcher_id
  string_of_int(id_int) ++ " " ++ string_of_bool(dis.active) ++ " " ++ string_of_int(CueSet.cardinal(dis.cue_queue))
}

let create(dispatcher_id): t = {
  dispatcher_id: DispatcherId(dispatcher_id),
  cue_queue: CueSet.empty,
  active: false
}
