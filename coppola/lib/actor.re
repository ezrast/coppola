type actor('data) = {
  data: 'data,
  stage: Stage.t,
  dispatcher_id: Dispatcher.DispatcherId.t,
}

let spawn(data: 'data) = {
  let stage = switch (Domain.DLS.get(Stage.dls_key)) {
  | None => raise(Errors.NotRunning)
  | Some(stage) => stage
  };
  let dispatcher_id = Stage.add_dispatcher(stage);
  let actor = { data, stage, dispatcher_id };
  actor
}

let send(actor, fn: 'data => unit): unit = {
  Stage.enqueue(
    actor.stage,
    actor.dispatcher_id,
    () => fn(actor.data),
  )
}

let call(actor, fn: 'data => 'ret): 'ret = {
  let (rr, ww) = Unix.pipe(~cloexec=true, ())
  let bytes = Bytes.init(1, _ => Char.chr(0))
  let ret = ref(None)

  Stage.enqueue(
    actor.stage,
    actor.dispatcher_id,
    () => {
      ret := try (Some(Ok(fn(actor.data)))) {
      | exn =>
        let bt = Printexc.get_raw_backtrace()
        Some(Error((exn, bt)))
      };
      assert(0 < Effect.perform @@ Async.Write({ fd: ww, bytes, offset: 0, length: 1, timeout_ms: -1}))
    },
  )

  let bytes = Bytes.init(1, _ => Char.chr(0))
  assert(0 < Effect.perform @@ Async.Read({ fd: rr, bytes, offset: 0, length: 1, timeout_ms: -1 }))
  Unix.close(ww);
  Unix.close(rr);
  switch (ret^) {
  | Some(Ok(value)) => value
  | Some(Error((exn, bt))) => Printexc.raise_with_backtrace(exn, bt)
  | None => raise(Failure("Incomplete"))
  }
}

module Infix {
  let (>>) = send;
  let (|>>) = call;
}
