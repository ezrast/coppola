/** FdStateMachine.t keeps track of the current state of each file
descriptor that has been registered for reading, writing, or listening.
Functions in this module should get called in response to various I/O-
related events, such as:
* Receiving an I/O effect from the user
* Being signaled by the operating system that an FD is available for IO
* An I/O task (i.e. a read or write) being completed
The iomux data itself is not contained in this structure because it
cannot be updated atomically. Instead, operations may add `iomux_op`s to
an internal queue that can be retrieved and cleared with
`fetch_iomux_ops`.
*/
type unit_continuation = Effect.Deep.continuation(unit, unit);
type int_continuation = Effect.Deep.continuation(int, unit);
type socket_handler = (Unix.file_descr, Unix.sockaddr) => unit;

type iomux_op =
  | RegisterRead
  | RegisterWrite
  | RegisterBoth
  | ExpungeRead
  | ExpungeWrite
  | ExpungeBoth
  | ExpungeBothNoCleanup;

module Wakeup: {
  type t;
};

type io_suspension = {
  bytes: bytes,
  offset: int,
  length: int,
  cont: Effect.Deep.continuation(int, unit),
  fd: Unix.file_descr,
  wakeup: Wakeup.t,
};

type t;
let create: unit => Atomic.t(t);

let fetch_iomux_ops: Atomic.t(t) => list((iomux_op, Unix.file_descr));

let perform_sleep: (
  int,
  Dispatcher.DispatcherId.t,
  Effect.Deep.continuation(unit, unit),
  Atomic.t(t)
) => unit;

type get_timed_out_result =
  | NoWakeupUntil(int)
  | LaunchWakeupTask(unit_continuation)
  | LaunchTimeoutTask(int_continuation);
let get_timed_out: Atomic.t(t) => get_timed_out_result;

let perform_read: (
  bytes,
  int,
  int,
  Effect.Deep.continuation(int, unit),
  int,
  Dispatcher.DispatcherId.t, Atomic.t(t), Unix.file_descr
) => result(unit, exn);

let perform_write: (
  bytes,
  int,
  int,
  Effect.Deep.continuation(int, unit),
  int,
  Dispatcher.DispatcherId.t, Atomic.t(t), Unix.file_descr
) => result(unit, exn);

let perform_listen: (
  (Unix.file_descr, Unix.sockaddr) => unit,
  Dispatcher.DispatcherId.t,
  Atomic.t(t),
  Unix.file_descr
) => unit;

type perform_close_result =
  | BecomeCloseTask
  | DoNothing;
let perform_close: (
  unit_continuation,
  Atomic.t(t),
  Unix.file_descr
) => result(perform_close_result, exn);

type expunge_receive_read_result =
  | LaunchReadTask(io_suspension)
  | LaunchHandler(socket_handler, Dispatcher.DispatcherId.t);
let expunge_receive_read: (
  Atomic.t(t),
  Unix.file_descr
) => expunge_receive_read_result;

type expunge_receive_write_result = LaunchWriteTask(io_suspension);
let expunge_receive_write: (
  Atomic.t(t),
  Unix.file_descr
) => expunge_receive_write_result;

type expunge_cancel_result =
  | LaunchIoResumeTasksAndClose(int_continuation, int_continuation)
  | LaunchIoResumeTaskAndClose(int_continuation)
  | LaunchIoResumeTask(int_continuation);
let expunge_cancel: (
  Atomic.t(t),
  Unix.file_descr
) => expunge_cancel_result;

type io_task_result =
  | ResumeIo
  | LaunchCloseTaskThenResumeIo;
let write_task: (Atomic.t(t), Unix.file_descr) => io_task_result;
let read_task: (Atomic.t(t), Unix.file_descr) => io_task_result;

type close_task_result = ResumeClose(unit_continuation);
let close_task: (Atomic.t(t), Unix.file_descr) => close_task_result;
