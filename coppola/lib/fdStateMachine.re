type unit_continuation = Effect.Deep.continuation(unit, unit);
type int_continuation = Effect.Deep.continuation(int, unit);
type socket_handler = (Unix.file_descr, Unix.sockaddr) => unit;

type iomux_op =
  | RegisterRead
  | RegisterWrite
  | RegisterBoth
  | ExpungeRead
  | ExpungeWrite
  | ExpungeBoth
  | ExpungeBothNoCleanup;

/** Wakeup.t represents a dispatcher that has been removed from
* scheduling due to waiting on IO or a sleep() call. */
module Wakeup = {
  type kind =
    | Sleep(Effect.Deep.continuation(unit, unit))
    | Write(Unix.file_descr)
    | Read(Unix.file_descr)

  and t = {
    wakeup_ms: int,
    dispatcher_id: Dispatcher.DispatcherId.t,
    kind,
  };

  let compare(aa, bb) = {
    // Sorting by wakeup time only makes sense for suspensions that actually wake up
    assert(aa.wakeup_ms >= 0);
    assert(bb.wakeup_ms >= 0);

    if (aa.wakeup_ms == bb.wakeup_ms) {
      Dispatcher.DispatcherId.compare(aa.dispatcher_id, bb.dispatcher_id)
    } else {
      Int.compare(aa.wakeup_ms, bb.wakeup_ms)
    }
  }
};

module WakeupSet = Set.Make(Wakeup);

type io_suspension = {
  bytes,
  offset: int,
  length: int,
  cont: Effect.Deep.continuation(int, unit),
  fd: Unix.file_descr,
  wakeup: Wakeup.t,
};

type open_state =
  | Null
  | Polling(io_suspension)
  | IoTaskInFlight

type close_state =
  | ExpungeThenClose(io_suspension)
  | IoTaskInFlightThenClose
  | ClosePending;

type fd_state =
  | Open(open_state, open_state)
  | Closing(unit_continuation, close_state, close_state)
  | Listening((Unix.file_descr, Unix.sockaddr) => unit, Dispatcher.DispatcherId.t)
  | ClosingListen(unit_continuation, (Unix.file_descr, Unix.sockaddr) => unit, Dispatcher.DispatcherId.t);

type t = {
  by_fd: FdMap.t(fd_state),
  by_wakeup: WakeupSet.t,
  ops: list((iomux_op, Unix.file_descr))
}

let create(): Atomic.t(t) = {
  Atomic.make({
    by_fd: FdMap.empty,
    by_wakeup: WakeupSet.empty,
    ops: []
  })
};

let rec update(fn, fd_states: Atomic.t(t), fd) = {
  let { by_fd: old_by_fd, by_wakeup: old_by_wakeup, ops: old_ops } as old_atom = Atomic.get(fd_states);
  let old_fdstate = old_by_fd |> FdMap.find_opt(fd) |> Option.value(~default=Open(Null, Null))
  switch (fn(old_fdstate, old_by_wakeup)) {
  | Ok((new_fdstate, new_by_wakeup, new_op, ret)) =>
    let new_ops = switch new_op {
    | Some(op) => [(op, fd), ...old_ops]
    | None => old_ops
    }
    let new_fdmap = switch new_fdstate {
    | Open(Null, Null) => old_by_fd |> FdMap.remove(fd)
    | _ => old_by_fd |> FdMap.add(fd, new_fdstate)
    }
    if (Atomic.compare_and_set(
      fd_states,
      old_atom,
      { by_fd: new_fdmap, by_wakeup: new_by_wakeup, ops: new_ops },
    )) {
      Ok(ret)
    } else {
      update(fn, fd_states, fd)
    }
  | Error(err) => Error(err)
  }
};

let rec update_infallible(fn, fd_states: Atomic.t(t), fd) = {
  let { by_fd: old_by_fd, by_wakeup: old_by_wakeup, ops: old_ops } as old_atom = Atomic.get(fd_states);
  let old_fdstate = old_by_fd |> FdMap.find_opt(fd) |> Option.value(~default=Open(Null, Null))
  let (new_fdstate, new_by_wakeup, new_op, ret) = fn(old_fdstate, old_by_wakeup)
  let new_ops = switch new_op {
  | Some(op) => [(op, fd), ...old_ops]
  | None => old_ops
  }
  let new_fdmap = switch new_fdstate {
  | Open(Null, Null) => old_by_fd |> FdMap.remove(fd)
  | _ => old_by_fd |> FdMap.add(fd, new_fdstate)
  }
  if (Atomic.compare_and_set(
    fd_states,
    old_atom,
    { by_fd: new_fdmap, by_wakeup: new_by_wakeup, ops: new_ops },
  )) {
    ret
  } else {
    update_infallible(fn, fd_states, fd)
  }
};

let now_ms() = Int64.div(Mtime_clock.now_ns(), 1_000_000L) |> Int64.to_int;

let rec perform_sleep(wakeup_ms, dispatcher_id, sleep_cont, fd_states: Atomic.t(t)) = {
  let sleep_susp = {
    Wakeup.wakeup_ms,
    dispatcher_id,
    kind: Sleep(sleep_cont)
  };
  let { by_fd, by_wakeup: old_by_wakeup, ops } as old_atom = Atomic.get(fd_states);
  let new_by_wakeup = old_by_wakeup |> WakeupSet.add(sleep_susp)
  if (Atomic.compare_and_set(fd_states, old_atom, { by_fd, by_wakeup: new_by_wakeup, ops })) {
    ()
  } else {
    perform_sleep(wakeup_ms, dispatcher_id, sleep_cont, fd_states)
  }
}

type get_timed_out_result =
  | NoWakeupUntil(int)
  | LaunchWakeupTask(unit_continuation)
  | LaunchTimeoutTask(int_continuation);

let rec get_timed_out(fd_states: Atomic.t(t)) = {
  let { by_fd: old_by_fd, by_wakeup: old_by_wakeup, ops: old_ops } as old_atom = Atomic.get(fd_states);

  switch (WakeupSet.min_elt_opt(old_by_wakeup)) {
  | None => NoWakeupUntil(-1) // No suspensions have timeouts set
  | Some(elt) when elt.wakeup_ms > now_ms() => NoWakeupUntil(elt.wakeup_ms) // No suspensions are ready to wake up
  | Some(elt) =>
    // A suspension has timed out and is ready to wake
    let new_by_wakeup = old_by_wakeup |> WakeupSet.remove(elt);
    // TODO one last non-blocking read to make sure data hasn't come in
    // at the last moment

    switch (elt.kind) {
    | Sleep(sleep_cont) =>
      if (Atomic.compare_and_set(
        fd_states,
        old_atom,
        { by_fd: old_by_fd, by_wakeup: new_by_wakeup, ops: old_ops },
      )) {
        LaunchWakeupTask(sleep_cont)
      } else {
        get_timed_out(fd_states)
      }
    | Read(fd) =>
      switch (old_by_fd |> FdMap.find_opt(fd)) {
      | Some(Open(Polling(read_susp), write_state)) =>
        let new_ops = [(ExpungeRead, fd), ...old_ops];
        let new_by_fd = old_by_fd |> FdMap.add(fd, Open(IoTaskInFlight, write_state))

        if (Atomic.compare_and_set(
          fd_states,
          old_atom,
          { by_fd: new_by_fd, by_wakeup: new_by_wakeup, ops: new_ops },
        )) {
          LaunchTimeoutTask(read_susp.cont)
        } else {
          get_timed_out(fd_states)
        }
      | Some(Open(Null, _)) => failwith("Coppola internal error: received timeout for FD in Null state")
      | Some(Open(IoTaskInFlight, _)) => failwith("Coppola internal error: received timeout for FD in IoTaskInFlight state")
      | Some(Listening(_)) => failwith("Coppola internal error: received timeout for FD in Listening state")
      | Some(ClosingListen(_)) => failwith("Coppola internal error: received timeout for FD in Listening state")
      | Some(Closing(_)) => failwith("Coppola internal error: received timeout for FD in Closing state")
      | None => failwith("Coppola internal error: No read FD found")
      }
    | Write(fd) =>
      switch (old_by_fd |> FdMap.find_opt(fd)) {
      | Some(Open(read_state, Polling(write_susp))) =>
        let new_ops = [(ExpungeWrite, fd), ...old_ops];
        let new_by_fd = old_by_fd |> FdMap.add(fd, Open(read_state, IoTaskInFlight))
        if (Atomic.compare_and_set(
          fd_states,
          old_atom,
          { by_fd: new_by_fd, by_wakeup: new_by_wakeup, ops: new_ops },
        )) {
          LaunchTimeoutTask(write_susp.cont)
        } else {
          get_timed_out(fd_states)
        }
      | Some(Open(_, Null)) => failwith("Coppola internal error: received timeout for FD in Null state")
      | Some(Open(_, IoTaskInFlight)) => failwith("Coppola internal error: received timeout for FD in IoTaskInFlight state")
      | Some(Listening(_)) => failwith("Coppola internal error: received timeout for FD in Listening state")
      | Some(ClosingListen(_)) => failwith("Coppola internal error: received timeout for FD in ClosingListen state")
      | Some(Closing(_)) => failwith("Coppola internal error: received timeout for FD in Closing state")
      | None => failwith("Coppola internal error: No write FD found")
      }
    }
  }
}

let rec fetch_iomux_ops(fd_states) = {
  let old_atom = Atomic.get(fd_states);
  if (Atomic.compare_and_set(fd_states, old_atom, { ...old_atom, ops: [] })) {
    old_atom.ops
  } else {
    fetch_iomux_ops(fd_states)
  }
}

let perform_read(bytes, offset, length, cont, wakeup_ms, dispatcher_id, fd_states, fd) = {
  update(_, fd_states, fd) @@ (state: fd_state, by_wakeup: WakeupSet.t) => {
    switch (state) {
    | Open(Null, ww) =>
      let susp = {
        bytes,
        offset,
        length,
        cont,
        fd,
        wakeup: {
          Wakeup.wakeup_ms, dispatcher_id, kind: Read(fd)
        },
      }
      Ok((
        Open(Polling(susp), ww),
        if (wakeup_ms >= 0) {by_wakeup |> WakeupSet.add(susp.wakeup)} else by_wakeup,
        Some(switch ww { | Polling(_) => RegisterBoth | _ => RegisterRead }),
        ()
      ))
    | Open(Polling(_), _)
    | Open(IoTaskInFlight, _)
    | Listening(_)
    | ClosingListen(_) => Error(Errors.FileDescriptorInUse)
    | Closing(_) => Error(Errors.FileDescriptorClosed)
    }
  }
};

let perform_write(bytes, offset, length, cont, wakeup_ms, dispatcher_id, fd_states, fd) = {
  update(_, fd_states, fd) @@ (state: fd_state, by_wakeup: WakeupSet.t) => {
    switch (state) {
    | Open(rr, Null) =>
      let susp = {
        bytes,
        offset,
        length,
        cont,
        fd,
        wakeup: {
          Wakeup.wakeup_ms, dispatcher_id, kind: Write(fd)
        },
      }
      Ok((
        Open(rr, Polling(susp)),
        if (wakeup_ms >= 0) {by_wakeup |> WakeupSet.add(susp.wakeup)} else by_wakeup,
        Some(switch rr { | Polling(_) => RegisterBoth | _ => RegisterWrite }),
        ()
      ))
    | Open(_, Polling(_))
    | Open(_, IoTaskInFlight)
    | Listening(_)
    | ClosingListen(_) => Error(Errors.FileDescriptorInUse)
    | Closing(_) => Error(Errors.FileDescriptorClosed)
    }
  }
};

let perform_listen(handler, dispatcher_id) = update_infallible @@ (state: fd_state, by_wakeup: WakeupSet.t) => {
  switch (state) {
  | Open(Null, Null) =>
    (
      Listening(handler, dispatcher_id),
      by_wakeup,
      Some(RegisterRead),
      (),
    )
  // By the time we've gotten here, we've already succeeded at a call to Unix.listen on this fd, so it should be
  // impossible for us to be in any other state - barring POSIX semantics edge cases, which are entirely likely
  | Open(_, _) => failwith("Coppola internal error: Tried to perform_listen while in Open state")
  | Listening(_) => failwith("Coppola internal error: Tried to perform_listen while in Listening state")
  | ClosingListen(_) => failwith("Coppola internal error: Tried to perform_listen while in ClosingListen state")
  | Closing(_, _, _) => failwith("Coppola internal error: Tried to perform_listen while in Closing state")
  }
}

type perform_close_result =
  | BecomeCloseTask
  | DoNothing;

// The user has performed a Close effect
let perform_close(close_cont) = update @@ (state: fd_state, by_wakeup: WakeupSet.t) => {
  switch (state) {
  | Open(Null, Null) =>
    // If no IO has been registered, the thread that received the effect can close the fd right away
    Ok((
      Closing(close_cont, ClosePending, ClosePending),
      by_wakeup,
      None,
      BecomeCloseTask,
    ))
  | Listening(handler, dispatcher_id) =>
    Ok((
      ClosingListen(close_cont, handler, dispatcher_id),
      by_wakeup,
      Some(ExpungeBothNoCleanup),
      BecomeCloseTask,
    ))

  | Open(rr, ww) =>
    let (new_rr, new_by_wakeup) = switch rr {
    | Null => (ClosePending, by_wakeup)
    | Polling(read_susp) => (ExpungeThenClose(read_susp), by_wakeup |> WakeupSet.remove(read_susp.wakeup))
    | IoTaskInFlight => (IoTaskInFlightThenClose, by_wakeup)
    }
    let (new_ww, new_new_by_wakeup) = switch ww {
    | Null => (ClosePending, new_by_wakeup)
    | Polling(write_susp) => (ExpungeThenClose(write_susp), new_by_wakeup |> WakeupSet.remove(write_susp.wakeup))
    | IoTaskInFlight => (IoTaskInFlightThenClose, new_by_wakeup)
    }
    // If IO has been registered, we mark it for expungement from iomux. A Close Task will be launched later as part of expungement.
    Ok((
      Closing(close_cont, new_rr, new_ww),
      new_new_by_wakeup,
      Some(ExpungeBoth),
      DoNothing
    ))
  | Closing(_)
  | ClosingListen(_) => Error(Errors.FileDescriptorClosed)
  }
};

type expunge_receive_read_result =
  | LaunchReadTask(io_suspension)
  | LaunchHandler(socket_handler, Dispatcher.DispatcherId.t)

let expunge_receive_read = update_infallible @@ (state: fd_state, by_wakeup: WakeupSet.t) => {
  switch (state) {
  | Open(Null, _) => failwith("Coppola internal error: FD in Null state tried to expunge_receive")
  | Open(Polling(read_susp), ww) => (Open(IoTaskInFlight, ww), by_wakeup |> WakeupSet.remove(read_susp.wakeup), None, LaunchReadTask(read_susp))
  | Open(IoTaskInFlight, _) => failwith("Coppola internal error: FD in IoTaskInFlight state tried to expunge_receive")
  | Closing(close_cont, ExpungeThenClose(read_susp), ww) => (Closing(close_cont, IoTaskInFlightThenClose, ww), by_wakeup, None, LaunchReadTask(read_susp))
  | Closing(_, ClosePending, _) => failwith("Coppola internal error: FD in ClosePending state tried to expunge_receive")
  | Closing(_, IoTaskInFlightThenClose, _) => failwith("Coppola internal error: FD in IoTaskInFlightThenClose state tried to expunge_receive")
  | Listening(handler, dispatcher_id) => (Listening(handler, dispatcher_id), by_wakeup, None, LaunchHandler(handler, dispatcher_id))
  | ClosingListen(close_cont, handler, dispatcher_id) => (ClosingListen(close_cont, handler, dispatcher_id), by_wakeup, None, LaunchHandler(handler, dispatcher_id))
  };
}

type expunge_receive_write_result =
  | LaunchWriteTask(io_suspension)

let expunge_receive_write = update_infallible @@ (state: fd_state, by_wakeup: WakeupSet.t) => {
  switch (state) {
  | Open(_, Null) => failwith("Coppola internal error: FD in Null state tried to expunge_receive")
  | Open(rr, Polling(write_susp)) => (Open(rr, IoTaskInFlight), by_wakeup |> WakeupSet.remove(write_susp.wakeup), None, LaunchWriteTask(write_susp))
  | Open(_, IoTaskInFlight) => failwith("Coppola internal error: FD in IoTaskInFlight state tried to expunge_receive")
  | Closing(close_cont, rr, ExpungeThenClose(write_susp)) => (Closing(close_cont, rr, IoTaskInFlightThenClose), by_wakeup, None, LaunchWriteTask(write_susp))
  | Closing(_, _, ClosePending) => failwith("Coppola internal error: FD in ClosePending state tried to expunge_receive")
  | Closing(_, _, IoTaskInFlightThenClose) => failwith("Coppola internal error: FD in IoTaskInFlightThenClose state tried to expunge_receive")
  | Listening(_) =>  failwith("Coppola internal error: FD in IoTaskInFlightThenClose state tried to expunge_receive_write")
  | ClosingListen(_) =>  failwith("Coppola internal error: FD in IoTaskInFlightThenClose state tried to expunge_receive_write")
  };
}

type expunge_cancel_result =
  | LaunchIoResumeTasksAndClose(int_continuation, int_continuation)
  | LaunchIoResumeTaskAndClose(int_continuation)
  | LaunchIoResumeTask(int_continuation);

let expunge_cancel = update_infallible @@ (state: fd_state, by_wakeup: WakeupSet.t) => {
  switch state {
  | Open(_, _) => failwith("Coppola internal error: FD in Open state tried to expunge_cancel")
  | Closing(close_cont, ExpungeThenClose(read_susp), ExpungeThenClose(write_susp)) =>
    (Closing(close_cont, ClosePending, ClosePending), by_wakeup, None, LaunchIoResumeTasksAndClose(read_susp.cont, write_susp.cont))

  | Closing(close_cont, ExpungeThenClose(read_susp), ClosePending) =>
    (Closing(close_cont, ClosePending, ClosePending), by_wakeup, None, LaunchIoResumeTaskAndClose(read_susp.cont))

  | Closing(close_cont, ClosePending, ExpungeThenClose(write_susp)) =>
    (Closing(close_cont, ClosePending, ClosePending), by_wakeup, None, LaunchIoResumeTaskAndClose(write_susp.cont))

  | Closing(close_cont, ExpungeThenClose(read_susp), IoTaskInFlightThenClose) =>
    (Closing(close_cont, ClosePending, IoTaskInFlightThenClose), by_wakeup, None, LaunchIoResumeTask(read_susp.cont))

  | Closing(close_cont, IoTaskInFlightThenClose, ExpungeThenClose(write_susp)) =>
    (Closing(close_cont, IoTaskInFlightThenClose, ClosePending), by_wakeup, None, LaunchIoResumeTask(write_susp.cont))

  | Closing(_, _, _) => failwith("Coppola internal error: FD not in ExpungeThenClose state tried to expunge_cancel")
  | Listening(_) => failwith("Coppola internal error: FD in Listening state tried to expunge_cancel")
  | ClosingListen(_) => failwith("Coppola internal error: FD in ClosingListen state tried to expunge_cancel")
  }
}

type io_task_result =
  | ResumeIo
  | LaunchCloseTaskThenResumeIo;

let write_task = update_infallible @@ (state: fd_state, by_wakeup: WakeupSet.t) => {
  switch (state) {
  | Open(_, Null) => failwith("Coppola internal error: FD in Null state tried to write_task")
  | Open(read_state, IoTaskInFlight) =>
    (Open(read_state, Null), by_wakeup, None, ResumeIo)
  | Open(_, Polling(_)) => failwith("Coppola internal error: FD in Null state tried to write_task")
  | Closing(close_cont, IoTaskInFlightThenClose, IoTaskInFlightThenClose as write_state)
  | Closing(close_cont, IoTaskInFlightThenClose, ExpungeThenClose(_) as write_state) =>
    (Closing(close_cont, ClosePending, write_state), by_wakeup, None, ResumeIo)
  | Closing(close_cont, IoTaskInFlightThenClose, ClosePending) =>
    (Closing(close_cont, ClosePending, ClosePending), by_wakeup, None, LaunchCloseTaskThenResumeIo)
  | Closing(_, ExpungeThenClose(_), _) => failwith("Coppola internal error: FD in ExpungeThenClose state tried to write_task")
  | Closing(_, ClosePending, _) => failwith("Coppola internal error: FD in ClosePending state tried to write_task")
  | Listening(_) => failwith("Coppola internal error: FD in Listening state tried to write_task")
  | ClosingListen(_) => failwith("Coppola internal error: FD in ClosingListen state tried to write_task")
  }
};

let read_task = update_infallible @@ (state: fd_state, by_wakeup: WakeupSet.t) => {
  switch (state) {
  | Open(Null, _) => failwith("Coppola internal error: FD in Null state tried to read_task")
  | Open(IoTaskInFlight, write_state) =>
    (Open(Null, write_state), by_wakeup, None, ResumeIo)
  | Open(Polling(_), _) => failwith("Coppola internal error: FD in Null state tried to read_task")
  | Closing(close_cont, IoTaskInFlightThenClose as read_state, IoTaskInFlightThenClose)
  | Closing(close_cont, ExpungeThenClose(_) as read_state, IoTaskInFlightThenClose) =>
    (Closing(close_cont, read_state, ClosePending), by_wakeup, None, ResumeIo)
  | Closing(close_cont, ClosePending, IoTaskInFlightThenClose) =>
    (Closing(close_cont, ClosePending, ClosePending), by_wakeup, None, LaunchCloseTaskThenResumeIo)
  | Closing(_, _, ExpungeThenClose(_)) => failwith("Coppola internal error: FD in ExpungeThenClose state tried to read_task")
  | Closing(_, _, ClosePending) => failwith("Coppola internal error: FD in ClosePending state tried to read_task")
  | Listening(_) => failwith("Coppola internal error: FD in Listening state tried to read_task")
  | ClosingListen(_) => failwith("Coppola internal error: FD in ClosingListen state tried to read_task")
  }
};

type close_task_result =
  // Only one case here, but we keep it to maintain the pattern of
  // reminding the consumer what it is expected to do with the result
  | ResumeClose(unit_continuation);

let close_task = update_infallible @@ (state: fd_state, by_wakeup: WakeupSet.t) => {
  switch (state) {
  | Open(_, _) => failwith("Coppola internal error: FD in Open state tried to close_task")
  | Closing(close_cont, ClosePending, ClosePending) => (Open(Null, Null), by_wakeup, None, ResumeClose(close_cont))
  | Closing(_, _, _) => failwith("Coppola internal error: FD not in ClosePending state tried to close_task")
  | Listening(_) => failwith("Coppola internal error: FD in Listening state tried to close_task")
  | ClosingListen(close_cont, _, _) => (Open(Null, Null), by_wakeup, None, ResumeClose(close_cont))
  }
};
