type Effect.t(_) +=
| Sleep({ timeout_ms: int }): Effect.t(unit)
| Read({ fd: Unix.file_descr, bytes, offset: int, length: int, timeout_ms: int }): Effect.t(int)
| Write({ fd: Unix.file_descr, bytes, offset: int, length: int, timeout_ms: int }): Effect.t(int)
| Listen({ fd: Unix.file_descr, handler: (Unix.file_descr, Unix.sockaddr) => unit }): Effect.t(unit)
| Close({ fd: Unix.file_descr }): Effect.t(unit)
| CreateProcess({ path: string, args: array(string), stdin: Unix.file_descr, stdout: Unix.file_descr, stderr: Unix.file_descr, handler: Unix.process_status => unit }): Effect.t(unit)
| Diagnostics: Effect.t(list((string, int))) // Not for public use; subject to breaking changes
