/** A Stage is where all the actors do their work, get it?
 * This module handles scheduling logic, activity queueing, and async
 * IO. */
module CueSet = Dispatcher.CueSet;
module IntMap = Map.Make(Int);
module DispatcherSet = Set.Make(Dispatcher);
module MPSC = Saturn_lockfree.Single_consumer_queue;

let rec pop_atomic_stack(stack: Atomic.t(list('a))) = {
  let old_stack = Atomic.get(stack)
  switch old_stack {
  | [] => None
  | [hd, ...tl] =>
    if (Atomic.compare_and_set(stack, old_stack, tl)) {
      Some(hd)
    } else {
      pop_atomic_stack(stack)
    }
  }
}

let rec push_atomic_stack(stack: Atomic.t(list('a)), item) = {
  let old_stack = Atomic.get(stack)
  let new_stack = [item, ...old_stack]
  if (!Atomic.compare_and_set(stack, old_stack, new_stack)) {
    push_atomic_stack(stack, item)
  }
}

let restart_on_eintr = Util.restart_on_eintr;

/** A task is a unit of work that can be completed by a system thread in
  * parallel. Anything related to polling for I/O (excluding the actual
  * read/write call) requires synchronization behind a mutex, so is done as
  * part of the work-finding loop and not represented here. */
type task =
  | Continue(Effect.Deep.continuation(unit, unit))
  | ContinueInt(Effect.Deep.continuation(int, unit), int)
  | CompleteRead(FdStateMachine.io_suspension)
  | CompleteWrite(FdStateMachine.io_suspension)
  | CompleteClose(Unix.file_descr)
  | CheckCues
  | CheckProcesses;

/** dispatchers holds all of the dispatchers associated with a stage,
 * indexed in two ways: by ID (used for enqueuing cues sent by actors)
 * and by readiness (used by work threads looking for tasks). Operations
 * on these collections must take care to always keep them in sync. */
module DispatcherMap = Ephemeron.K1.Make(Dispatcher.DispatcherId);

type dispatchers = {
  by_id: DispatcherMap.t(Dispatcher.t),
  mutable by_readiness: DispatcherSet.t,
};

let edit_dispatcher(old: Dispatcher.t, nu: Dispatcher.t, dispatchers) = {
  assert (old.dispatcher_id == nu.dispatcher_id)
  let data': Dispatcher.t = DispatcherMap.find(dispatchers.by_id, old.dispatcher_id)
  assert (CueSet.equal(data'.cue_queue, old.cue_queue))
  assert (data'.active == old.active)

  if (nu.active == false && CueSet.is_empty(nu.cue_queue)) {
    // Dispatchers that may never be active again should be removed from
    // by_readiness so that they don't inadvertently keep entries alive
    // in the by_id ephemeron
    dispatchers.by_readiness = dispatchers.by_readiness
      |> DispatcherSet.remove(old)
  } else {
    dispatchers.by_readiness = dispatchers.by_readiness
      |> DispatcherSet.remove(old)
      |> DispatcherSet.add(nu)
  }
  DispatcherMap.replace(dispatchers.by_id, old.dispatcher_id, nu)
};

/** The whole scheduler; instantiated once per call to `Coppola.run` */
type t = {
  dispatchers: Exclusion.t(dispatchers),
  notification_pipe_r: Unix.file_descr,
  notification_pipe_w: Unix.file_descr,
  next_dispatcher_id: Atomic.t(int),
  next_cue_id: Atomic.t(int),
  failed: Atomic.t(bool),
  task_stack: Atomic.t(list(task)),
  processes: Atomic.t(IntMap.t((Dispatcher.DispatcherId.t, Unix.process_status => unit))),
  cues_ready: Atomic.t(bool),
  processes_ready: Atomic.t(bool),
  fd_states: Atomic.t(FdStateMachine.t),
  iomux: Exclusion.t(IomuxWrapper.t),
};

let make() = {
  let (notification_pipe_r, notification_pipe_w) = Unix.pipe(~cloexec=true, ());
  // set_nonblock shouldn't be necessary, but in the event of a bug, it's
  // easier to diagnose EAGAIN than a hang
  Unix.set_nonblock(notification_pipe_r);
  {
    dispatchers: Exclusion.create({
      by_id: DispatcherMap.create(16),
      by_readiness: DispatcherSet.empty,
    }),
    notification_pipe_r,
    notification_pipe_w,
    next_dispatcher_id: Atomic.make(0),
    next_cue_id: Atomic.make(0),
    failed: Atomic.make(false),
    task_stack: Atomic.make([]),
    processes: Atomic.make(IntMap.empty),
    cues_ready: Atomic.make(false),
    processes_ready: Atomic.make(false),
    fd_states: FdStateMachine.create(),
    iomux: Exclusion.create(IomuxWrapper.create(notification_pipe_r)),
  }
}

// Call this when you need to bump the current thread out of the select
// operation in check_io. This can be because:
// 1. The set of sleep suspensions has been added to
// 2. The set of IO suspensions has been added to (or, in theory,
//    removed from, but this happens inside the select mutex anyway)
// 3. The dispatcher list has been modified in any way and should be
//    checked for readiness or program completion
// 4. All dispatchers are idle and the run should terminate
let break_select(stage) = {
  // Currently letting this be blocking because the notification pipe should always be available for writing
  // On my machine, the OS buffers 64 kiB on each pipe so there's no risk of the pipe filling up
  // TODO research if there are other failure modes here that I'm not accounting for
  assert(0 < restart_on_eintr @@ () => Unix.write(stage.notification_pipe_w, IomuxWrapper.notification_write_buf, 0, 1));
}

let enqueue(stage, dispatcher_id, proc) = {
  let cue_id = Util.get_and_increment(stage.next_cue_id);
  let cue = Cue.{cue_id, proc}
  Exclusion.protect(stage.dispatchers, dispatchers => {
    // enqueue should only be called on dispatchers guaranteed to be present in the DispatcherMap
    let old_dispatcher = DispatcherMap.find(dispatchers.by_id, dispatcher_id)
    let new_dispatcher = { ...old_dispatcher, cue_queue: CueSet.add(cue, old_dispatcher.cue_queue) }
    edit_dispatcher(old_dispatcher, new_dispatcher, dispatchers)
    Atomic.set(stage.cues_ready, true)
    break_select(stage)
  })
}

let check_io(stage) = {
  if (Atomic.get(stage.cues_ready)) {
    Atomic.set(stage.cues_ready, false);
    Some(CheckCues)
  } else if (Atomic.get(stage.processes_ready)) {
    Atomic.set(stage.processes_ready, false);
    Some(CheckProcesses)
  } else {
    switch (FdStateMachine.get_timed_out(stage.fd_states)) {
    | LaunchTimeoutTask(cont) => Some(ContinueInt(cont, -1))
    | LaunchWakeupTask(cont) => Some(Continue(cont))
    | NoWakeupUntil(wakeup_ms) =>
      Exclusion.protect(stage.iomux, iomux => {
        let ops = FdStateMachine.fetch_iomux_ops(stage.fd_states)
        let rec process_ops_in_fifo_order(list) = {
          switch list {
          | [] => ()
          | [(op: FdStateMachine.iomux_op, fd), ...tl] =>
            process_ops_in_fifo_order(tl)
            switch (op) {
            | RegisterRead => IomuxWrapper.add_read(iomux, fd)
            | RegisterWrite => IomuxWrapper.add_write(iomux, fd)
            | RegisterBoth => IomuxWrapper.upgrade_to_rw(iomux, fd)
            | ExpungeRead =>
              IomuxWrapper.invalidate_read(iomux, fd)
            | ExpungeWrite =>
              IomuxWrapper.invalidate_write(iomux, fd)
            | ExpungeBoth =>
              IomuxWrapper.invalidate_both(iomux, fd)
              switch (FdStateMachine.expunge_cancel(stage.fd_states, fd)) {
              | LaunchIoResumeTask(cont) =>
                push_atomic_stack(stage.task_stack, ContinueInt(cont, -2))
              | LaunchIoResumeTaskAndClose(cont) =>
                push_atomic_stack(stage.task_stack, ContinueInt(cont, -2))
                push_atomic_stack(stage.task_stack, CompleteClose(fd))
              | LaunchIoResumeTasksAndClose(cont1, cont2) =>
                push_atomic_stack(stage.task_stack, ContinueInt(cont1, -2))
                push_atomic_stack(stage.task_stack, ContinueInt(cont2, -2))
                push_atomic_stack(stage.task_stack, CompleteClose(fd))
              }
            | ExpungeBothNoCleanup =>
              IomuxWrapper.invalidate_both(iomux, fd)
            }
          }
        }
        process_ops_in_fifo_order(ops)
        let () = (IomuxWrapper.select(
          iomux,
          wakeup_ms,
          ~on_read=fd => {
            switch (FdStateMachine.expunge_receive_read(stage.fd_states, fd)) {
            | LaunchReadTask(susp) =>
              push_atomic_stack(stage.task_stack, CompleteRead(susp));
              true
            | LaunchHandler(handler, dispatcher_id) =>
              switch (restart_on_eintr @@ () => Unix.accept(~cloexec=true, fd)) {
              | exception _ => () // Assume fd has been invalidated by user
              | (client_fd, client_sockaddr) =>
                // push_atomic_stack(stage.task_stack, Cue({dispatcher_id, proc: () => handler(client_fd, client_sockaddr)}));
                enqueue(stage, dispatcher_id, () => handler(client_fd, client_sockaddr))
              };
              false
            }
          },
          ~on_write=fd => {
            let LaunchWriteTask(susp) = FdStateMachine.expunge_receive_write(stage.fd_states, fd);
            push_atomic_stack(stage.task_stack, CompleteWrite(susp));
          },
        ))
        None
      })
    };
  }
}

let perform_cue(stage, dispatcher_id, proc) = {
  Effect.Deep.match_with(proc, (), {
    retc: () =>
      Exclusion.protect(stage.dispatchers, dispatchers => {
        let old_dispatcher = DispatcherMap.find(dispatchers.by_id, dispatcher_id);
        assert(old_dispatcher.active)
        let new_dispatcher = {...old_dispatcher, active: false }
        edit_dispatcher(old_dispatcher, new_dispatcher, dispatchers)
        Atomic.set(stage.cues_ready, true);
        break_select(stage);
      }),
    effc: (type a, eff: Effect.t(a)) => {
      switch eff {
      | Async.Sleep({ timeout_ms }) =>
        Some((cont: Effect.Deep.continuation(a, _)) => {
          let wakeup = Util.now_ms() + timeout_ms
          FdStateMachine.perform_sleep(wakeup, dispatcher_id, cont, stage.fd_states)
          break_select(stage);
        })
      | Async.Read({ fd, bytes, offset, length, timeout_ms }) =>
        Some((cont: Effect.Deep.continuation(a, _)) => {
          if (fd == stage.notification_pipe_r || fd == stage.notification_pipe_w) {
            Effect.Deep.discontinue(cont, Errors.FileDescriptorBelongsToCoppola)
          } else {
            let wakeup = if (timeout_ms < 0) {-1} else {Util.now_ms() + timeout_ms}
            switch (FdStateMachine.perform_read(bytes, offset, length, cont, wakeup, dispatcher_id, stage.fd_states, fd)) {
            | Ok() => break_select(stage);
            | Error(err) => Effect.Deep.discontinue(cont, err)
            }
          }
        })
      | Async.Write({ fd, bytes, offset, length, timeout_ms }) =>
        Some((cont: Effect.Deep.continuation(a, _)) => {
          if (fd == stage.notification_pipe_r || fd == stage.notification_pipe_w) {
            Effect.Deep.discontinue(cont, Errors.FileDescriptorBelongsToCoppola)
          } else {
            let wakeup = if (timeout_ms < 0) {-1} else {Util.now_ms() + timeout_ms}
            switch (FdStateMachine.perform_write(bytes, offset, length, cont, wakeup, dispatcher_id, stage.fd_states, fd)) {
            | Ok() => break_select(stage);
            | Error(err) => Effect.Deep.discontinue(cont, err)
            }
          }
        })
      | Async.Listen({ fd, handler }) =>
        Some((cont: Effect.Deep.continuation(a, _)) => {
          if (fd == stage.notification_pipe_r || fd == stage.notification_pipe_w) {
            Effect.Deep.discontinue(cont, Errors.FileDescriptorBelongsToCoppola)
          } else {
            switch (Unix.listen(fd, 4096)) { // TODO make receive queue size configurable
            | exception err => Effect.Deep.discontinue(cont, err)
            | () =>
              let () = FdStateMachine.perform_listen(handler, dispatcher_id, stage.fd_states, fd)
              break_select(stage);
              Effect.Deep.continue(cont, ())
            };
          }
        })
      | Async.Close({ fd }) =>
        Some((cont: Effect.Deep.continuation(a, _)) => {
          if (fd == stage.notification_pipe_r || fd == stage.notification_pipe_w) {
            Effect.Deep.discontinue(cont, Errors.FileDescriptorBelongsToCoppola)
          } else {
            switch (FdStateMachine.perform_close(cont, stage.fd_states, fd)) {
            | Ok(BecomeCloseTask) =>
              break_select(stage);
              switch (Unix.close(fd)) {
              | exception err =>
                let ResumeClose(_cont) = FdStateMachine.close_task(stage.fd_states, fd)
                Effect.Deep.discontinue(cont, err)
              | () =>
                let ResumeClose(_cont) = FdStateMachine.close_task(stage.fd_states, fd)
                Effect.Deep.continue(cont, ())
              }
            | Ok(DoNothing) => break_select(stage);
            | Error(err) => Effect.Deep.discontinue(cont, err)
            }
          }
        })
      | Async.CreateProcess({ path, args, stdin, stdout, stderr, handler }) =>
        Some((cont: Effect.Deep.continuation(a, _)) => {
          switch (Unix.create_process(path, args, stdin, stdout, stderr)) {
          | exception err => Effect.Deep.discontinue(cont, err)
          | pid =>
            let rec loop() = {
              let old_processes = Atomic.get(stage.processes)
              let new_processes = old_processes |> IntMap.add(pid, (dispatcher_id, handler))
              if (!Atomic.compare_and_set(stage.processes, old_processes, new_processes)) { loop() }
            }
            loop()
            Effect.Deep.continue(cont, ())
          }
          break_select(stage);
        })
      | Async.Diagnostics =>
        Some((cont: Effect.Deep.continuation(a, _)) => {
          let actor_count = Exclusion.protect(stage.dispatchers, dispatchers => {
            DispatcherMap.clean(dispatchers.by_id)
            DispatcherMap.length(dispatchers.by_id)
          });

          Effect.Deep.continue(cont, [(("actor_count"), actor_count)])
        })
      | _ => None
      }
    },
    exnc: raise,
  })
}

let perform_work(stage, work) = {
  switch work {
  | Continue(cont) =>
    Effect.Deep.continue(cont, ());
  | ContinueInt(cont, int) =>
    Effect.Deep.continue(cont, int);
  | CheckCues =>
    let cue_check_result = Exclusion.protect(stage.dispatchers, dispatchers => {
      // stage.dispatchers is ordered first by idle-ness (i.e. not running an action already) and then by readiness
      // (i.e. are there cues in the queue). An idle, ready dispatcher will always be first in the cue, while if the
      // first and last dispatchers are both idle and non-ready (or if there are no dispatchers) then no actions are
      // in flight, meaning no messages can ever be sent to enqueue more cues, so we should terminate the run.
      switch (DispatcherSet.min_elt_opt(dispatchers.by_readiness)) {
      | Some({ active: true, _ }) => `NoReadyDispatchers
      | None => `AllDispatchersIdle
      | Some(old_dispatcher) =>
        switch (CueSet.min_elt_opt(old_dispatcher.cue_queue)) {
        | Some(cue) =>
          // An idle actor has an action queued; dispatch it
          let new_dispatcher: Dispatcher.t = {
            dispatcher_id: old_dispatcher.dispatcher_id,
            active: true,
            cue_queue: CueSet.remove(cue, old_dispatcher.cue_queue)
          }
          edit_dispatcher(old_dispatcher, new_dispatcher, dispatchers);
          `FoundReadyDispatcher(old_dispatcher.dispatcher_id, cue)
        | None =>
          assert (!DispatcherSet.max_elt(dispatchers.by_readiness).active);
          `AllDispatchersIdle
        }
      }
    })

    switch cue_check_result {
    | `AllDispatchersIdle => Atomic.set(stage.failed, true)
    | `NoReadyDispatchers => () // TODO rename
    | `FoundReadyDispatcher(dispatcher_id, { proc, _ }) =>
      Atomic.set(stage.cues_ready, true)
      break_select(stage)
      perform_cue(stage, dispatcher_id, proc)
    }
  | CheckProcesses =>
    let should_break = ref(false)
    let rec loop() = {
      switch (Unix.waitpid([Unix.WNOHANG], -1)) { // -1 only supported on Linux
      | exception Unix.Unix_error(Unix.EINTR, _, _) => loop()
      | exception Unix.Unix_error(Unix.ECHILD, _, _)
      | (0, _) => () // Probably another thread got here before us
      | (_, Unix.WSTOPPED(_)) => loop() // We don't care about stopped children
      | (pid, Unix.WEXITED(_) as status)
      | (pid, Unix.WSIGNALED(_) as status) =>

        let rec loop2() = {
          let old_processes = Atomic.get(stage.processes)
          switch (IntMap.find_opt(pid, old_processes)) {
          | None => ()
          | Some((dispatcher_id, callback)) =>
            let new_processes = IntMap.remove(pid, old_processes)
            if (!Atomic.compare_and_set(stage.processes, old_processes, new_processes)) {
              loop2()
            } else {
              enqueue(stage, dispatcher_id, () => callback(status))
            }
          }
        }
        loop2()
        should_break := true
        // Keep checking for children in case the OS has coalesced multiple SIGCHLD signals into one
        loop()
      }
    }
    loop()
    if (should_break^) {
      Atomic.set(stage.processes_ready, true)
      break_select(stage)
    }
  | CompleteRead({fd, bytes, offset, length, cont, wakeup: _ }) =>
    switch (restart_on_eintr @@ () => Unix.read(fd, bytes, offset,length)) {
    | exception err =>
      switch (FdStateMachine.read_task(stage.fd_states, fd)) {
      | ResumeIo => ()
      | LaunchCloseTaskThenResumeIo => push_atomic_stack(stage.task_stack, CompleteClose(fd))
      }
      Effect.Deep.discontinue(cont, err)
    | bytes_read =>
      switch (FdStateMachine.read_task(stage.fd_states, fd)) {
      | ResumeIo => ()
      | LaunchCloseTaskThenResumeIo => push_atomic_stack(stage.task_stack, CompleteClose(fd))
      }
      Effect.Deep.continue(cont, bytes_read)
    }
  | CompleteWrite({fd, bytes, offset, length, cont, wakeup: _ }) =>
    switch (restart_on_eintr @@ () => Unix.single_write(fd, bytes, offset, length)) {
    | exception err =>
      switch (FdStateMachine.write_task(stage.fd_states, fd)) {
      | ResumeIo => ()
      | LaunchCloseTaskThenResumeIo => push_atomic_stack(stage.task_stack, CompleteClose(fd))
      }
      Effect.Deep.discontinue(cont, err)
    | bytes_written =>
      assert(bytes_written > 0);
      switch (FdStateMachine.write_task(stage.fd_states, fd)) {
      | ResumeIo => ()
      | LaunchCloseTaskThenResumeIo => push_atomic_stack(stage.task_stack, CompleteClose(fd))
      }
      Effect.Deep.continue(cont, bytes_written)
    }
  | CompleteClose(fd) =>
    switch (Unix.close(fd)) {
    | exception err =>
      let ResumeClose(close_cont) = FdStateMachine.close_task(stage.fd_states, fd)
      break_select(stage);
      Effect.Deep.discontinue(close_cont, err)
    | () =>
      let ResumeClose(close_cont) = FdStateMachine.close_task(stage.fd_states, fd)
      break_select(stage);
      Effect.Deep.continue(close_cont, ())
    }
  }
}

let rec work_loop(stage) = {
  switch (pop_atomic_stack(stage.task_stack)) {
  | Some(task) => Some(task)
  | None => check_io(stage)
  }
  |> Option.iter(perform_work(stage))

  if (!Atomic.get(stage.failed)) { work_loop(stage) }
}

let add_dispatcher(stage) = {
  let id = Util.get_and_increment(stage.next_dispatcher_id)
  let dispatcher = Dispatcher.create(id)
  Exclusion.protect(stage.dispatchers, dispatchers => {
    DispatcherMap.add(dispatchers.by_id, dispatcher.dispatcher_id, dispatcher)
  });
  dispatcher.dispatcher_id
}

let dls_key: Domain.DLS.key(option(t)) = Domain.DLS.new_key(() => None);

let run(~domain_count=?, init) = {
  if (Domain.DLS.get(dls_key) != None) raise(Errors.AlreadyRunning);
  let stage = make()

  let old_sigchild = Sys.signal(Sys.sigchld, Signal_handle(_ => { Atomic.set(stage.processes_ready, true); break_select(stage) }))
  let old_sigpipe = Sys.signal(Sys.sigpipe, Signal_ignore)

  // klugey one-off; we don't usually want to make dispatchers unassociated with actors
  enqueue(stage, add_dispatcher(stage), init)
  let domain_count = Option.value(domain_count, ~default=Domain.recommended_domain_count());

  let finally() = {
    // FdStateMachine.snapshot(stage.fd_states)
    // |> FdMap.iter((fd, state: FdStateMachine.fd_state) => {
    //   switch state {
    //   | Listening(_)
    //   | ClosingListen(_) =>
    //     try (Unix.close(fd)) { | _ => () }
    //   | _ => ()
    //   }
    // })

    Domain.DLS.set(dls_key, None)
    Sys.set_signal(Sys.sigchld, old_sigchild)
    Sys.set_signal(Sys.sigchld, old_sigpipe)
  }

  Fun.protect(~finally, () => {
    let do_work() = {
      Domain.DLS.set(dls_key, Some(stage));

      // TODO: This catches both unhandled errors from user code, and those generated by the scheduler. Setting
      // stage.failed and calling break_select should cause all remaining scheduler domains to eventually terminate, but
      // because exceptional control flow is invisible it's hard to guarantee that scheduler invariants are always
      // respected. The system may hang in some edge cases that are not worth finding until the code is more mature.
      try (work_loop(stage)) {
      | exn =>
        let bt = Printexc.get_raw_backtrace()
        Atomic.set(stage.failed, true)
        break_select(stage)
        Printexc.raise_with_backtrace(exn, bt)
      }
      Atomic.set(stage.failed, true);
      break_select(stage); // Let the next thread know not to wait around in the work loop
    }

    let domains = List.init(domain_count - 1, _idx => Domain.spawn(do_work))
    do_work()

    domains |> List.iter(Domain.join)
  })
}

