type t('a) = {
  mutex: Mutex.t,
  wrapped: 'a,
}

let protect = (excl, fn) => {
  // OCaml doesn't interrupt a mutex wait to handle signals, which can
  // cause the scheduler to deadlock if SIGCHLD is assigned to a blocked
  // thread. We solve this by masking SIGCHLD during every mutex wait,
  // forcing the runtime to deliver the signal to a different thread.
  // Use try_lock first to avoid the extra sigprocmask syscalls in the
  // we-have-work-to-do-right-now case
  if (!Mutex.try_lock(excl.mutex)) {
    ignore @@ Unix.sigprocmask(Unix.SIG_BLOCK, [Sys.sigchld]);
    Fun.protect(
      () => Mutex.lock(excl.mutex),
      ~finally=() => ignore @@ Unix.sigprocmask(Unix.SIG_UNBLOCK, [Sys.sigchld]),
    );
  }

  Fun.protect(
    ~finally=() => Mutex.unlock(excl.mutex),
    () => fn(excl.wrapped)
  )
};

let create(wrapped) = {
  mutex: Mutex.create(),
  wrapped
}
