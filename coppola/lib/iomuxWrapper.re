type t = {
  iomux: Iomux.Poll.t,
  mutable idx_by_fd: FdMap.t(int),
  mutable iomux_size: int,
  notification_pipe_r: Unix.file_descr,
}

let notification_read_buf_length = 32 // basically arbitrary
let notification_read_buf = Domain.DLS.new_key(() => Bytes.create(notification_read_buf_length))
let notification_write_buf = Bytes.make(1, Char.chr(0));

let create(notification_pipe_r) = {
  let iomux = Iomux.Poll.create(~maxfds=1024, ())
  Iomux.Poll.set_index(iomux, 0, notification_pipe_r, Iomux.Poll.Flags.pollin);
  {
    iomux,
    idx_by_fd: FdMap.empty,
    iomux_size: 1,
    notification_pipe_r,
  }
}

let add_read(wrapper, fd) = {
  Iomux.Poll.set_index(wrapper.iomux, wrapper.iomux_size, fd, Iomux.Poll.Flags.pollin)
  wrapper.idx_by_fd = wrapper.idx_by_fd |> FdMap.add(fd, wrapper.iomux_size);
  wrapper.iomux_size = wrapper.iomux_size + 1; // TODO check for maxfds overrun
}

let add_write(wrapper, fd) = {
  Iomux.Poll.set_index(wrapper.iomux, wrapper.iomux_size, fd, Iomux.Poll.Flags.pollout)
  wrapper.idx_by_fd = wrapper.idx_by_fd |> FdMap.add(fd, wrapper.iomux_size);
  wrapper.iomux_size = wrapper.iomux_size + 1; // TODO check for maxfds overrun
}

let upgrade_to_rw(wrapper, fd) = {
  let idx = wrapper.idx_by_fd |> FdMap.find(fd)
  Iomux.Poll.set_index(wrapper.iomux, idx, fd, Iomux.Poll.Flags.(pollin + pollout))
}

let invalidate_both_at_idx(wrapper, fd, idx) = {
  assert(idx < wrapper.iomux_size)
  wrapper.iomux_size = wrapper.iomux_size - 1;

  let last_fd = Iomux.Poll.get_fd(wrapper.iomux, wrapper.iomux_size)
  let last_events = Iomux.Poll.get_events(wrapper.iomux, wrapper.iomux_size)

  wrapper.idx_by_fd = wrapper.idx_by_fd |> FdMap.remove(fd) |> FdMap.add(last_fd, idx);
  Iomux.Poll.set_index(wrapper.iomux, idx, last_fd, last_events)
  Iomux.Poll.invalidate_index(wrapper.iomux, wrapper.iomux_size)
}

let invalidate_both(wrapper, fd) = {
  let idx = FdMap.find(fd, wrapper.idx_by_fd)
  invalidate_both_at_idx(wrapper, fd, idx)
}

let invalidate_read_at_idx(wrapper, fd, idx) = {
  assert(idx < wrapper.iomux_size)
  let old_events = Iomux.Poll.get_events(wrapper.iomux, idx)
  if (Iomux.Poll.Flags.mem(old_events, Iomux.Poll.Flags.pollout)) {
    Iomux.Poll.set_index(wrapper.iomux, idx, fd, Iomux.Poll.Flags.pollout)
  } else {
    invalidate_both_at_idx(wrapper, fd, idx)
  }
}

let invalidate_read(wrapper, fd) = {
  let idx = FdMap.find(fd, wrapper.idx_by_fd)
  invalidate_read_at_idx(wrapper, fd, idx)
}

let invalidate_write_at_idx(wrapper, fd, idx) = {
  assert(idx < wrapper.iomux_size)
  let old_events = Iomux.Poll.get_events(wrapper.iomux, idx)
  if (Iomux.Poll.Flags.mem(old_events, Iomux.Poll.Flags.pollin)) {
    Iomux.Poll.set_index(wrapper.iomux, idx, fd, Iomux.Poll.Flags.pollin)
  } else
    invalidate_both_at_idx(wrapper, fd, idx)
  }

let invalidate_write(wrapper, fd) = {
  let idx = FdMap.find(fd, wrapper.idx_by_fd)
  invalidate_write_at_idx(wrapper, fd, idx)
}

let rec select(wrapper, wakeup_ms, ~on_read, ~on_write) = {
  let module Poll = Iomux.Poll;

  let timeout_ms = if (wakeup_ms < 0) { -1 } else Int.max(0, wakeup_ms - Util.now_ms());
  let timeout = Poll.Milliseconds(timeout_ms)
  switch (Poll.poll(wrapper.iomux, wrapper.iomux_size, timeout)) {
  | exception Unix.Unix_error(Unix.EINTR, _, _) => select(wrapper, wakeup_ms, ~on_read, ~on_write)
  | ready_count =>
    let rec loop(idx, left_to_find) = {
      if (left_to_find > 0) {
        assert (idx >= 0)
        let revents = Poll.get_revents(wrapper.iomux, idx)
        if (Poll.Flags.empty == revents) {
          loop(idx - 1, left_to_find)
        } else {
          if (Poll.Flags.(mem (pollin, revents))) {
            let fd = Poll.get_fd(wrapper.iomux, idx);
            assert(Iomux.Util.fd_of_unix(fd) >= 0)
            if (fd == wrapper.notification_pipe_r) {
              assert (0 < Util.restart_on_eintr @@ () => Unix.read(fd, Domain.DLS.get(notification_read_buf), 0, notification_read_buf_length));
            } else {
              if(on_read(fd)) { invalidate_read_at_idx(wrapper, fd, idx) }
            }
          };
          if (Poll.Flags.(mem (pollout, revents))) {
            let fd = Poll.get_fd(wrapper.iomux, idx);
            assert(Iomux.Util.fd_of_unix(fd) >= 0)
            invalidate_write_at_idx(wrapper, fd, idx)
            on_write(fd)
          };
          loop(idx - 1, left_to_find - 1);
        }
      }
    }
    loop(wrapper.iomux_size - 1, ready_count)
  };
}
