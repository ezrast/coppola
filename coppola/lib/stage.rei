type t;

let dls_key: Domain.DLS.key(option(t));
let make: unit => t;
let run: ~domain_count:int=? => (unit => unit) => unit;
let enqueue: t => Dispatcher.DispatcherId.t => (unit => unit) => unit;
let add_dispatcher: t => Dispatcher.DispatcherId.t;
