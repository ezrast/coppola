/** A dispatcher holds information associated with the scheduling of a
 * particular actor, and dispatchers are mostly one-to-one with actors.
 * Essentially, it's the implementation-facing side of an actor, while
 * Actor.Make.t is the user-facing side. */

// DispatcherId.t is used as a key for an ephemeron hash table in Stage.
// When a DispatcherId is no longer reachable, it cannot have further
// behaviors enqueued, so it is safe to remove from the table. This is
// why the id must be a boxed value and not a plain int, and why other
// dispatcher data must be held in a separate Anon.t record - so that it
// may be passed around in other places inside the scheduler without
// preventing the id from being garbage-collected.

module CueSet: (module type of Set.Make(Cue));

module DispatcherId: {
  type t;
  let hash: t => int;
  let equal: t => t => bool;
  let compare: t => t => int;
};

type t = {
  dispatcher_id: DispatcherId.t,
  cue_queue: CueSet.t,
  active: bool,
}

let compare: t => t => int;
let string_of_dispatcher: t => string;
let create: int => t;
