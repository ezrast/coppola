let rec get_and_increment(atomic_int) = {
  let id = Atomic.get(atomic_int)
  if (Atomic.compare_and_set(atomic_int, id, id + 1)) {
    id
  } else {
    get_and_increment(atomic_int)
  }
}

let rec restart_on_eintr(fn) = {
  try (fn()) {
  | Unix.Unix_error(Unix.EINTR, _, _) => restart_on_eintr(fn)
  }
};

let now_ms() = Int64.div(Mtime_clock.now_ns(), 1_000_000L) |> Int64.to_int;
