type t = {
  cue_id: int,
  proc: unit => unit,
};

let compare: t => t => int
