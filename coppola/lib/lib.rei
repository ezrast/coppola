module Async: (module type of Async);
module Errors: (module type of Errors);

let run: ~domain_count:int=? => (unit => unit) => unit;

type actor('data);
let spawn: 'data => actor('data);
let send: actor('data) => ('data => unit) => unit;
let call: actor('data) => ('data => 'ret) => 'ret;

module Infix {
  let (>>): actor('data) => ('data => unit) => unit;
  let (|>>): actor('data) => ('data => 'ret) => 'ret;
}
