type t = {
  cue_id: int,
  proc: unit => unit,
};

let compare(c1, c2) = {
  Int.compare(c1.cue_id, c2.cue_id)
}
