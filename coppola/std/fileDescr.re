module Coppola = Lib;

class closer(~auto_close, fd) {
  as _;
  pri on_close = { try (Unix.close(fd)) { | Unix.Unix_error(EBADF, _, _) => ()}};
  inherit Io.closer(~auto_close);
};

class virtual seeker(fd) {
  as self;
  pri virtual clear_buffer: unit;
  pub get_seek(offset) = {
    let ret = Unix.lseek(fd, offset, Unix.SEEK_SET);
    self#clear_buffer;
    ret
  };
  pub get_seek_rel(offset) = {
    let ret = Unix.lseek(fd, offset, Unix.SEEK_CUR);
    self#clear_buffer;
    ret
  };

  pub get_seek_end(offset) = {
    let ret = Unix.lseek(fd, offset, Unix.SEEK_END);
    self#clear_buffer;
    ret
  };

  inherit Io.seeker;
};

class reader(~read_buffer_size=1024, ~auto_close=false, fd) {
  let bytes = Bytes.create(read_buffer_size);
  let actor = Coppola.spawn();
  as _;

  pri read = {
    open Coppola.Infix;
    let bytes_read = actor |>> () => Effect.perform(Coppola.Async.Read({fd, bytes, offset: 0, length: Bytes.length(bytes), timeout_ms: -1}));
    (bytes, bytes_read)
  };
  inherit Io.reader();
  inherit closer(~auto_close, fd);
};

class writer(~auto_close=false, fd) {
  let actor = Coppola.spawn();
  as _;

  pri write(bytes, offset, length) = {
    open Coppola.Infix;
    actor |>> () => Effect.perform(Coppola.Async.Write({fd, bytes, offset, length, timeout_ms: -1}))
  };
  inherit Io.writer();
  inherit closer(~auto_close, fd);
};

class rw(~read_buffer_size=1024, ~auto_close=false, fd) {
  let write_actor = Coppola.spawn();

  let bytes = Bytes.create(read_buffer_size);
  let read_actor = Coppola.spawn();
  as _;

  pri write(bytes, offset, length) = {
    open Coppola.Infix;
    write_actor |>> () => Effect.perform(Coppola.Async.Write({fd, bytes, offset, length, timeout_ms: -1}))
  };

  pri read = {
    open Coppola.Infix;
    let bytes_read = read_actor |>> () => Effect.perform(Coppola.Async.Read({fd, bytes, offset: 0, length: Bytes.length(bytes), timeout_ms: -1}));
    (bytes, bytes_read)
  };

  inherit Io.writer();
  inherit Io.reader();
  inherit closer(~auto_close, fd);
};

