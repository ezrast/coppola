module Coppola = Lib;

class reader(~read_buffer_size=1024, ~keep_exec=false, path) {
  let flags = [Unix.O_RDONLY];
  let flags = switch keep_exec {
  | true => [Unix.O_KEEPEXEC, ...flags]
  | false => [Unix.O_CLOEXEC, ...flags]
  };
  let fd = Unix.openfile(path, flags, 0); // TODO: error handling
  as _;

  inherit FileDescr.reader(~read_buffer_size, ~auto_close=true, fd);
  inherit FileDescr.seeker(fd);
};

type create_perms = [
  | `Must(int)
  | `May(int)
  | `MustNot
];

class writer(~create_perms: create_perms=`MustNot, ~truncate=false, ~append=false, ~keep_exec=false, path) {
  let flags = [Unix.O_RDWR];
  let flags = switch truncate {
  | true => [Unix.O_TRUNC, ...flags]
  | false => flags
  };
  let flags = switch append {
  | true => [Unix.O_APPEND, ...flags]
  | false => flags
  };
  let (flags, perms) = switch create_perms {
  | `Must(perms) => ([Unix.O_CREAT, Unix.O_EXCL, ...flags], perms)
  | `May(perms) => ([Unix.O_CREAT, ...flags], perms)
  | `MustNot => (flags, 0)
  };
  let flags = switch keep_exec {
  | true => [Unix.O_KEEPEXEC, ...flags]
  | false => [Unix.O_CLOEXEC, ...flags]
  };
  let fd = Unix.openfile(path, flags, perms); // TODO: error handling
  as _;

  inherit FileDescr.writer(~auto_close=true, fd);
  pri clear_buffer = ();
  inherit FileDescr.seeker(fd);
};

class rw(~read_buffer_size=1024, ~create_perms=`MustNot, ~truncate=false, ~append=false, ~keep_exec=false, path) {
  let flags = [Unix.O_RDWR];
  let flags = switch truncate {
  | true => [Unix.O_TRUNC, ...flags]
  | false => flags
  };
  let flags = switch append {
  | true => [Unix.O_APPEND, ...flags]
  | false => flags
  };
  let (flags, perms) = switch create_perms {
  | `Must(perms) => ([Unix.O_CREAT, Unix.O_EXCL, ...flags], perms)
  | `May(perms) => ([Unix.O_CREAT, ...flags], perms)
  | `MustNot => (flags, 0)
  };
  let flags = switch keep_exec {
  | true => [Unix.O_KEEPEXEC, ...flags]
  | false => [Unix.O_CLOEXEC, ...flags]
  };
  let fd = Unix.openfile(path, flags, perms); // TODO: error handling
  as _;
  inherit FileDescr.rw(~read_buffer_size, ~auto_close=true, fd);

  inherit FileDescr.seeker(fd);
};
