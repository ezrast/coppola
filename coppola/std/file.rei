type create_perms = [
  | `Must(int)
  | `May(int)
  | `MustNot
];

class reader: (~read_buffer_size: int=?, ~keep_exec: bool=?, string) => {
  inherit FileDescr.reader;
  inherit FileDescr.seeker;
};

class writer: (~create_perms: create_perms=?, ~truncate: bool=?, ~append: bool=?, ~keep_exec: bool=?, string) => {
  inherit FileDescr.writer;
  pri clear_buffer: unit;
  inherit FileDescr.seeker;
};

class rw: (~read_buffer_size: int=?, ~create_perms: create_perms=?, ~truncate: bool=?, ~append: bool=?, ~keep_exec: bool=?, string) => {
  inherit FileDescr.rw;
  inherit FileDescr.seeker;
};
