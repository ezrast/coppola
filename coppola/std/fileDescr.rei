class closer: (~auto_close: bool, Unix.file_descr) => {
  pri on_close: unit;
  inherit Io.closer;
};

class virtual seeker: (Unix.file_descr) => {
  pri virtual clear_buffer: unit;
  pub get_seek: (int) => int;
  pub get_seek_rel: (int) => int;
  pub get_seek_end: (int) => int;
  inherit Io.seeker;
};

class reader: (~read_buffer_size: int=?, ~auto_close: bool=?, Unix.file_descr) => {
  pri read: (bytes, int);
  inherit Io.reader;
  inherit closer;
};

class writer: (~auto_close: bool=?, Unix.file_descr) => {
  pri write: (bytes, int, int) => int;
  inherit Io.writer;
  inherit closer;
};

class rw: (~read_buffer_size: int=?, ~auto_close: bool=?, Unix.file_descr) => {
  pri read: (bytes, int);
  pri write: (bytes, int, int) => int;

  inherit Io.reader;
  inherit Io.writer;
  inherit closer;
};
