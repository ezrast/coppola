module File = File;
module FileDescr = FileDescr;
module Io = Io;

let sleep_ms(timeout_ms) = Effect.perform(Lib.Async.Sleep({timeout_ms: timeout_ms}));
