module Coppola = Lib;

class virtual reader() {
  let buf = ref(Bytes.empty);
  let bytes_read = ref(-1);
  let pos = ref(-1);
  as self;

  pri virtual read: (bytes, int);
  pub virtual is_closed: bool;

  pri clear_buffer = {
    bytes_read := -1
    pos := -1
  };

  pri do_read = {
    let (data', bytes_read') = self#read;
    buf := data';
    bytes_read := bytes_read';
    pos := 0
  };

  // TODO these methods all assume that once a read yields 0 bytes,
  // it will continue to do so forever, but that's way wrong yo
  pub read_char = {
    if (self#is_closed) None else {
      if (pos < bytes_read) {
        let ret = Bytes.get(buf^, pos^);
        pos := 1 + pos^;
        Some(ret)
      } else {
        if (bytes_read^ == 0) {
          None
        } else {
          self#do_read;
          if (bytes_read^ == 0) {
            None
          } else {
            pos := 1;
            Some(Bytes.get(buf^, 0))
          }
        }
      }
    }
  };

  pub iter_max(max, fn: string => unit) = {
    if (self#is_closed) () else {
      if (bytes_read^ == 0) {
        ()
      } else {
        let rec loop(remaining) = {
          let available = bytes_read^ - pos^
          if (available >= remaining) {
            fn(Bytes.sub_string(buf^, pos^, remaining))
            pos := remaining + pos^
          } else {
            fn(Bytes.sub_string(buf^, pos^, available))
            self#do_read
            if (bytes_read^ == 0) {
              ()
            } else {
              loop(remaining - available)
            }
          }
        };

        if (pos < bytes_read) {
          loop(max)
        } else {
          self#do_read
          if (bytes_read^ == 0) {
            ()
          } else {
            loop(max)
          }
        }
      };
    };
  };

  pub read_max(max) = {
    if (self#is_closed) None else {
      if (bytes_read^ == 0) {
        None
      } else {
        let rec loop(ret_buf, remaining) = {
          let available = bytes_read^ - pos^
          if (available >= remaining) {
            Buffer.add_subbytes(ret_buf, buf^, pos^, remaining);
            pos := remaining + pos^;
            Some(Buffer.contents(ret_buf))
          } else {
            Buffer.add_subbytes(ret_buf, buf^, pos^, available);
            self#do_read;
            if (bytes_read^ == 0) {
              Some(Buffer.contents(ret_buf))
            } else {
              loop(ret_buf, remaining - available)
            }
          }
        };

        if (pos < bytes_read) {
          loop(Buffer.create(max), max)
        } else {
          self#do_read
          if (bytes_read^ == 0) {
            None
          } else {
            loop(Buffer.create(max), max)
          }
        }
      };
    };
  };

  pub read_line = {
    if (self#is_closed) None else {
      if (bytes_read^ == 0) {
        None
      } else {
        let rec loop(ret_buf) = {
          let char = Bytes.unsafe_get(buf^, pos^)
          pos := 1 + pos^
          if (char == '\n') {
            Some(Buffer.contents(ret_buf))
          } else {
            Buffer.add_char(ret_buf, char)
            if (pos < bytes_read) {
              loop(ret_buf)
            } else {
              self#do_read
              if (bytes_read^ == 0) {
                Some(Buffer.contents(ret_buf))
              } else {
                loop(ret_buf)
              }
            }
          }
        };

        if (pos < bytes_read) {
          loop(Buffer.create(128))
        } else {
          self#do_read
          if (bytes_read^ == 0) {
            None
          } else {
            loop(Buffer.create(128))
          }
        }
      };
    };
  };

  pub read_line_sep = {
    if (self#is_closed) None else {
      if (bytes_read^ == 0) {
        None
      } else {
        let rec loop(ret_buf) = {
          let char = Bytes.unsafe_get(buf^, pos^)
          Buffer.add_char(ret_buf, char)
          pos := 1 + pos^
          if (char == '\n') {
            Some(Buffer.contents(ret_buf))
          } else {
            if (pos < bytes_read) {
              loop(ret_buf)
            } else {
              self#do_read
              if (bytes_read^ == 0) {
                Some(Buffer.contents(ret_buf))
              } else {
                loop(ret_buf)
              }
            }
          }
        };

        if (pos < bytes_read) {
          loop(Buffer.create(128))
        } else {
          self#do_read
          if (bytes_read^ == 0) {
            None
          } else {
            loop(Buffer.create(128))
          }
        }
      };
    };
  };

  pub line_seq {
    Seq.of_dispenser(() => self#read_line)
  };

  pub line_seq_sep {
    Seq.of_dispenser(() => self#read_line_sep)
  };
};

class virtual writer() {
  let char_buffer = Bytes.create(1);
  as self;

  pri virtual write: (bytes, int, int) => int;
  pub virtual is_closed: bool;

  pub write_char(char) = {
    // TODO handle is_closed
    Bytes.set(char_buffer, 0, char)
    ignore @@ self#write(char_buffer, 0, 1)
  };

  pub write_string(str) = {
    // TODO handle is_closed
    let bytes = Bytes.unsafe_of_string(str) // I think this is okay because we never modify the bytes
    self#unsafe_write_bytes(bytes, 0, Bytes.length(bytes))
  };

  pub unsafe_write_bytes(bytes, start, length) = {
    // TODO handle is_closed
    let rec loop(bytes_written) = {
      if (bytes_written < length) {
        let new_bytes_written = self#write(bytes, bytes_written, Bytes.length(bytes) - bytes_written)
        loop(bytes_written + new_bytes_written)
      }
    }
    loop(start)
  }
};

class virtual closer(~auto_close: bool) {
  let is_closed = ref(false);
  as self;

  pri virtual on_close: unit;

  pub close = {
    if (!is_closed^) {
      is_closed := true;
      self#on_close;
    }
  };
  pub is_closed = is_closed^;
  initializer { if (auto_close) Gc.finalise(obj => obj#close, self) };
};

class virtual seeker {
  as self;
  pub virtual get_seek: int => int;
  pub seek(offset) = ignore(self#get_seek(offset));

  pub virtual get_seek_end: int => int;
  pub seek_end(offset) = ignore(self#get_seek_end(offset));

  // TODO relative seeks are difficult because read buffers make the apparent cursor position earlier than the real one
  // pub seek_rel(offset) = ignore(self#get_seek_rel(offset));
  // pub virtual get_seek_rel: int => int;
};
