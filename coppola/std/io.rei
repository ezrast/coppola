class virtual reader: (unit) => {
  pri virtual read: (bytes, int);
  pri clear_buffer: unit;
  pub virtual is_closed: bool;

  pub read_char: option(char);
  pub read_max: int => option(string);
  pub read_line: option(string);
  pub read_line_sep: option(string);

  pub iter_max: (int, string => unit) => unit;
  pub line_seq: Seq.t(string);
  pub line_seq_sep: Seq.t(string);
};

class virtual writer: (unit) => {
  pri virtual write: (bytes, int, int) => int;
  pub virtual is_closed: bool;

  pub write_char: char => unit;
  pub write_string: string => unit;
  pub unsafe_write_bytes: (bytes, int, int) => unit;
};

class virtual closer: (~auto_close: bool) => {
  pri virtual on_close: unit;

  pub close: unit;
  pub is_closed: bool;
};

class virtual seeker: {
  pub virtual get_seek: int => int;
  pub virtual get_seek_end: int => int;
  pub seek: int => unit;
  pub seek_end: int => unit;
};
