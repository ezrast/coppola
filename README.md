# Coppola
*A multi-core scheduler for OCaml*

## What's this about then?
It's a library that handles parallelism and asynchronous I/O, inspired by actor models such as that popularized by Erlang. I say "inspired by" because Coppola is an absolutely minimal take on the concept: there are no paradigms for process supervision or error handling (an unhandled exception just terminates the scheduler, albeit somewhat gracefully) or ownership. You get to declare that certain data should be operated on sequentially, everything else gets distributed in parallel between a number of worker threads (usually one per CPU core), and that's about it.

There are no monads or colorful functions or anything. All your code just looks normal aside from the message-passing operators.

## How does it work?
First you call `Coppola.run` and pass it a task to execute. We use "task" to mean any function scheduled by the Coppola runtime (oh, and all my examples are written in the Reason dialect; sorry):
```reason
Coppola.run(() => {
  // Body of your program goes here; other Coppola functions and effects
  // will fail if they happen outside of a `run`
})
```
Then you spawn an actor and send it a task, which will—assuming you have CPU cores to spare—be executed in a separate thread :
```reason
let simple_actor = Coppola.spawn()
Coppola.send(simple_actor, () => {
  for (_ in 1 to 1_000_000) {
    let _do_expensive_work = 1 + 1
  }
})
print_endline("Super valuable work going on in the background while you read this")
```
If the worker threads are all busy, additional calls to `send` will get queued up, and Coppola's scheduler will process them as capacity becomes available. You can pass `~domain_count` to `run` to set the number of worker threads to something other than the number of cores you have, if you really want.

Actors are generic: you can pass whatever you want to `spawn`, and tasks sent to that actor will operate on that data. Each actor maintains its own queue of tasks that execute in order, one at a time, so the data structures you use don't need to be concurrency-safe as long as you are careful not to pass them, or any of their internals, between actors.
```reason
open Coppola;

let append(string, buffer) = Buffer.add_string(buffer, string)
let print_buffer(buffer) = print_endline(Buffer.contents(buffer))

let buffer_actor = spawn(Buffer.create(16))

send(buffer_actor, append("These strings are "))
send(buffer_actor, append("guaranteed to arrive in "))
send(buffer_actor, append("order, before any others"))
send(buffer_actor, print_buffer)

send(simple_actor, () => {
  print_endline("This message may print before, after, or intermixed with the other; ordering is not guaranteed between different actors")
})
```
You can get data back by using `call` instead of `send`, which will suspend the current task and wait for a result:
```reason
let buffer_contents = call(buffer_actor, Buffer.contents)
```

The `Infix` module defines `>>` and `|>>` as shortcuts for `send` and `call`:
```reason
open Coppola.Infix
simple_actor >> () => {
  Unix.sleep(2)
  print_endline("Tick tock!")
}
let buffer_length = buffer_actor |>> Buffer.length
```
That's it! That's the whole parallelism API. Just `spawn`, `send`, and `call`, plus a single boilerplate `run` call per program.

## You said something about handling I/O though
Oh yeah, there's an `Async` module with a handful of effects (as in OCaml 5's effects system) useful for doing otherwise-blocking things asynchronously. They are very low-level: just enough for another library to provide convenient APIs built on top.

Until that library exists, you can perform the effects directly. For example:
```reason
Effect.perform(Coppola.Async.Sleep(2.0))
```
* `Sleep` just suspends the task for a while. From your code's perspective, it behaves just like `Unix.sleepf`, but it frees up the physical thread for other tasks in the meantime. You can have an unbounded number of actors sleeping without occupying the runtime's worker threads.
* `Read` and `Write` are like `Unix.read` and `Unix.single_write`, but with an extra timeout parameter (use -1 for no timeout). If an operation cannot be completed right away, the task gets suspended until it can. If the timeout elapses, the effect returns -1; otherwise, the number of bytes written.
* `Listen` takes a socket, which you should already have bound to an address, and a callback. Coppola will begin listening on the socket, and invoke the callback each time a client is accepted, passing the client socket and address (i.e., the result of the `Unix.accept` call). `Listen` always returns immediately, allowing your actor to confirm that the socket is fully set up. **However**, callback tasks are placed onto the listening actor's work queue as if by `send`, so any additional work done will delay handling of new connections. For a typical application, the callback should do nothing but spin up a fresh actor and send the client socket to it for handling.
* `Close` closes a socket that has previously been passed to `Listen`. It is an error to perform `Close` on a non-listening socket; for regular file descriptors just use `Unix.close` like normal.
* `CreateProcess` functions like `Unix.create_process`, except that it also takes a callback that will be invoked when the process terminates. As with `Listen`, the callback is assigned to the work queue of the actor that created the process, though since it only fires once, it is less imperative that you keep this actor unburdened with other tasks. The callback takes `Unix.process_status` as a parameter.

## Is it any good?
No.

I'm just writing this thing for fun, and it currently has several deficiencies:
* The aforementioned high-level IO library has not yet been written
* It is not extensively tested and probably crashes in a wide variety of edge cases
* It leaks resources
* It hasn't been benchmarked or optimized in any way

Go use [Riot](https://github.com/riot-ml/riot), probably. I mean, I don't know if Riot is any good either but it at least bears some semblance of being made by a professional.

## I want to use it anyway
Bless your heart. `opam pin` to this repo—because submitting to the actual Opam repository might suggest some kind of fitness for purpose that I'm not about to affirm—and be prepared for breaking changes.
